const { successSending, errorSending } = require('../shared/alert');

var nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport({
    service: 'Gmail',
    host: 'smtp.gmail.com',
    port: 587,
    secure: true,
    auth: {
        user: 'webilearn.contact@gmail.com',
        pass: 'Mpokora123'
    }
});


function sendMail(user, res, type) {
    var message;
    switch (type) {
        case 'new':
            message = newAdministratorTemplate(user);
            break;
        case 'recover':
            message = passwordRecoverTemplate(user);
            break;
        case 'recover':
            message = passwordChangedTemplate(user);
            break;
    }
    smtpTransport.sendMail(message, function (err, result) {
        console.log(result);
        if (err) {
            errorSending(res, 'Error', err)
        } else {
            smtpTransport.close();
            successSending(res, 'Done', 'Please check your email adress.');
        }
    });
}

function passwordRecoverTemplate(user) {

    let link = "http://localhost:4200/admin/reset/" + user.resetPasswordToken;
    const mailOptions = {
        from: process.env.FROM_EMAIL,
        to: user.email,
        subject: "WebiLearn account recover.",
        text: `Hi  \n 
        Please click on the following link ${link} to reset your password. This link will be no longer available in one hour. \n\n 
        If you did not request this, please ignore this email and your password will remain unchanged.\n`,
    }
    return mailOptions;

}

function newAdministratorTemplate(user) {
    return {
        from: process.env.FROM_EMAIL,
        to: user.email,
        subject: "Congrats, you are almost there.",
        text: `Hi our team have treated your request.\n
        Please try to connect to our site with these informations.\n
          Email : ${user.email}\n
          Password : ${user.password}\n
        Webilearn team.
          \n`
    };

}


function passwordChangedTemplate(user) {
    return {
        from: process.env.FROM_EMAIL,
        to: user.email,
        subject: "Your password has been changed",
        text: `Hi ${user.name} \n 
        This is a confirmation that the password for your account ${user.email} has just been changed.\n`
    };

}

exports.sendMail = sendMail;
