const Joi = require("@hapi/joi");
/**
 * Validate all Id for Database
 * @param {*} obj
 */

function validateId(obj) {
    const schema = Joi.object({
      id:Joi.string()
        .min(5)
        .required()
    });
    return schema.validate(obj);
  }
  /**
 * Validate all Id for Database
 * @param {*} obj
 */

function validateContentId(obj) {
  const schema = Joi.object({
    id:Joi.string()
      .min(5)
      .required()
  });
  return schema.validate(obj);
}
/**
 * Validate User object recived from token
 * @param {*} obj
 */

function validateToken(obj) {
  const schema = Joi.object({
    _id:Joi.string()
      .required(),
      userName:Joi.string()
      .required(),
      email:Joi.email()
      .required(),
      iat:Joi.number()
      .required(),
      exp:Joi.number()
      .required(),
  });
  return schema.validate(obj);
}
  exports.validateId = validateId;
  exports.validateContentId = validateContentId;