/**
 * Return Success to front
 * 
 * @param {Response} res 
 * @param {string} title 
 * @param {string} text 
 */
function successSending(res, title, text) {
  return res.status(200).json({
    title: title,
    text: text
  });
}
/**
 * Return errors to front
 * 
 * @param {Response} res 
 * @param {string} text 
 * @param {string} message 
 */
function errorSending(res, title, text) {
  return res.status(500).json({
    title: title,
    text: text
  });
}
/**
 * Return data to front
 * 
 * @param {Response} res 
 * @param {JSON} object 
 */
function dataSending(res, data) {
  return res.status(200).json({
    data: data
  });
}
/**
 * Exports
 */
exports.successSending = successSending;
exports.errorSending = errorSending;
exports.dataSending = dataSending;
