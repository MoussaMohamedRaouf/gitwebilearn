const multer  = require('multer');
var fs = require('fs');
var path = require('path');
const { v4: uuidv4 } = require('uuid');
const { errorSending } = require('../shared/alert');

function storageStrategy(destination) { 
return multer.diskStorage({
    destination:function(req,file,cb){
        const path = './uploads/';
        cb(null,path+destination+'/');
    },
    filename:function(req,file,cb){
        cb(null, uuidv4()+path.extname(file.originalname));
    }
});
}

function deleteFile(destination,fileName,res){
    fs.unlink(destination+'/'+fileName, function (err) {
        if (err) {errorSending(res,'Error',err);}
        console.log('File deleted');
    }); 
}




exports.storageStrategy = storageStrategy;
exports.deleteFile = deleteFile;
