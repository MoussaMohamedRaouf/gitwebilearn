const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const subscribtionController = require("../controllers/subscribtion");
/**
 *  Role routes
 */

router.get("/all", auth, subscribtionController.getSubscribtions);
router.get("/:id", auth, subscribtionController.getSubscribtion);
router.post("/new", auth, subscribtionController.newSubscribtion);
router.post("/delete/:id", auth, subscribtionController.deleteSubscribtion);
router.post("/update/:id", auth, subscribtionController.updateSubscribtion);


/**
 * @exports
 */
module.exports = router;
