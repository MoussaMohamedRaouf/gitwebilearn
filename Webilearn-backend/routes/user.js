const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const userController = require("../controllers/user");
/**
 *  Role routes
 */
router.get("/", userController.getUsers);
router.post("/addUser", userController.addUser);
router.post("/deleteUser", userController.deleteUser);
router.post("/updateUser", userController.updateUser);

/**
 * @exports
 */
module.exports = router;
