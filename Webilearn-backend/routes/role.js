const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const roleController = require("../controllers/role");
/**
 *  Role routes
 */

router.get("/all", auth, roleController.getRoles);
router.get("/allExceptMine/:id", auth, roleController.getRolesExceptMine);
router.get("/:id", auth, roleController.getRole);
router.post("/add", auth, roleController.addRole);
router.post("/delete", auth, roleController.deleteRole);
router.post("/update", auth, roleController.updateRole);

router.post("/addPrivToRole/:id", auth, roleController.addPrivilegeToRole);
router.post("/deletePrivToRole/:id", auth, roleController.deletePrivilegeToRole);

/**
 * @exports
 */
module.exports = router;
