const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const privilegeController = require("../controllers/privilege");
/**
 *  Role routes
 */
 router.get("/all", auth, privilegeController.getAllPrivileges);
 router.get("/:id", auth, privilegeController.getPrivilege);
 router.post("/new", auth, privilegeController.newPrivilege);
 router.post("/update/:id", auth, privilegeController.updatePrivilege);
 router.post("/delete/:id", auth, privilegeController.deletePrivilege);

/**
 * @exports
 */
module.exports = router;
