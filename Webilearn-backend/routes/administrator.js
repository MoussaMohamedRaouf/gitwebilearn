const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const role = require("../middelware/role");
const adminController = require("../controllers/administrator");
const multer  = require('multer');
const storing = require("../shared/storing");
const storageStrategy = storing.storageStrategy('profilePictures','profilePic');
const upload = multer({ storage:storageStrategy })
/**
 * Routes with no authentifications
 */
router.post("/signUp", adminController.adminSignUp);
router.post("/fakeAdministrators", adminController.fakerAdminSignUp);
router.post("/signIn", adminController.adminSignIn);
router.post("/recover", adminController.recover);
router.post("/validateResetToken", adminController.valdiatePasswordToken);
router.post("/verifySessionToken", adminController.validateSessionToken);
router.post("/reset/:token", adminController.resetPassword);

/**
 * Routes with authentification
 */
router.get("/me", auth, adminController.getAdmin);
router.post("/firstSignIn", auth, upload.single('wizardPicture'), adminController.firstSignInAdministrator);
router.post("/delete", auth, adminController.deleteAdministrator);
router.post("/update", auth, adminController.updateAdministrator);
router.post("/updatePassword", auth, adminController.updateAdministratorPassword);

router.post("/changeStatus", [auth,role], adminController.changeStatus);
router.get("/all", [auth,role], adminController.getAdministrators);
router.post("/new", [auth,role], adminController.createNewAdministrator);
router.post("/addRole/:id",[auth,role], adminController.addRoleToAdministrator);
router.post("/deleteRole/:id", [auth,role], adminController.deleteRoleToAdministrator);

/**
 * @exports
 */
module.exports = router;
