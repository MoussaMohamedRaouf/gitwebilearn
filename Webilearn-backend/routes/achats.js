const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const achatController = require("../controllers/achats");
/**
 *  Role routes
 */
router.get("/all", auth, achatController.getAllAchats);
router.get("/:id", auth, achatController.getOneAchat);
router.post("/new", auth, achatController.newAchat);
router.post("/update/:id", auth, achatController.updateAchat);
router.post("/delete/:id", auth, achatController.deleteAchat);


/**
 * @exports
 */
module.exports = router;
