const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const coursController = require("../controllers/cours");

/**
 *  Cours routes
 */
router.get("/all", auth, coursController.getAllcourses);
router.get("/allIds", auth, coursController.getAllcoursesIds);
router.get("/:id", auth, coursController.getCours);
 router.post("/fakeCourses", auth, coursController.fakerCoursesCreation);
 router.post("/new", auth, coursController.newCours);
 router.post("/update/:id", auth, coursController.updateCours);
 router.post("/delete/:id", auth, coursController.deleteCours);

 
/**
 * @exports
 */
module.exports = router;
