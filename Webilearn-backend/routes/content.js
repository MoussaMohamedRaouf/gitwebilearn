
const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const contentController = require("../controllers/content");
const storing = require("../shared/storing");

const multer  = require('multer');
const storageStrategy = storing.storageStrategy('courseContents')
const upload = multer({ storage:storageStrategy })


/**
 * Cours content
 */

router.get("/:id/content/all", auth, contentController.getOneCoursContents);
router.get("/:id/content/:contentId", auth, contentController.getOneCoursOneContent);
router.post("/:id/content/new", auth,  upload.single('content'),  contentController.newCoursContent);
router.post("/:id/content/update/:contentId", auth, upload.single('content'), contentController.updateCoursContent);
router.post("/:id/content/delete/:contentId", auth, contentController.deleteCoursContent);

/**
 * @exports
 */
module.exports = router;
