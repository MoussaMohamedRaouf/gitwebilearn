
const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const teacherController = require("../controllers/teacher");


/**
 * Cours content
 */
router.post("/changeStatus/:id", auth, teacherController.changeStatus);
router.post("/fakeTeachers", auth, teacherController.fakerTeachersCreation);

router.get("/all", auth, teacherController.getTeachers);
router.get("/allIds", auth, teacherController.getTeachersIds);

router.get("/:id", auth, teacherController.getOneTeacher);
router.post("/new", auth,    teacherController.newTeacher);
router.post("/update/:id", auth,  teacherController.updateTeacher);
router.post("/delete/:id", auth, teacherController.deleteTeacher);

/**
 * @exports
 */
module.exports = router;
