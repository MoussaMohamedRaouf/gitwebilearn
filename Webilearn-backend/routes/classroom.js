const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const classroomController = require("../controllers/classroom");
/**
 *  Role routes
 */
 router.get("/all", auth, classroomController.getAllClassrooms);
 router.get("/:id", auth, classroomController.getClassroom);
 router.post("/new", auth, classroomController.newClassroom);
 router.post("/update/:id", auth, classroomController.updateClassroom);
 router.post("/delete/:id", auth, classroomController.deleteClassroom);

/**
 * @exports
 */
module.exports = router;
