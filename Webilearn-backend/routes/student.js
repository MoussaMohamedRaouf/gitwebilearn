
const express = require("express");
const router = express.Router();
const auth = require("../middelware/auth");
const studentController = require("../controllers/student");


/**
 * Cours content
 */
router.post("/changeStatus/:id", auth, studentController.changeStatus);
router.post("/fakeStudents", auth, studentController.fakerStudentsCreation);
router.get("/all", auth, studentController.getStudents);
router.get("/withIds", auth, studentController.getStudentsIds);
router.get("/:id", auth, studentController.getOneStudent);
router.post("/new", auth,    studentController.newStudent);
router.post("/update/:id", auth,  studentController.updateStudent);
router.post("/delete/:id", auth, studentController.deleteStudent);

/**
 * @exports
 */
module.exports = router;
