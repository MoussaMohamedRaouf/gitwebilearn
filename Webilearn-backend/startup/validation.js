var cookieParser = require("cookie-parser");
var logger = require("morgan");
var createError = require("http-errors");
var path = require("path");
/**
 *  Validation
 */
module.exports = function(app, express) {
  // view engine setup
  app.set("views", path.join(__dirname, "views"));
  app.set("view engine", "ejs");

  app.use(logger("dev"));
  app.use('/uploads',express.static('uploads'));
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, "public")));

  /**
   *  Catch 404 and forward to error handler
   */

  app.use(function(req, res, next) {
    next(createError(404));
  });

  /**
   *  Error Handler
   */
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
  });
};
