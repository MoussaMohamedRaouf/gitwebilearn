const indexRouter = require("../routes/index");
const error = require("../middelware/error");
var express = require("express");

const administratorRouter = require("../routes/administrator");
const courseRouter = require("../routes/cours");
const contentRouter = require("../routes/content");
const subscribtionRouter = require("../routes/subscribtion");
const teachersRoute = require("../routes/teacher");
const achatsRoute = require("../routes/achats");
const classRoomRoute = require("../routes/classroom");
const studentsRoute = require("../routes/student");
const roleRouter = require("../routes/role");
const privilegeRouter = require("../routes/privilege");

module.exports = function(app) {
  app.use(express.json());
  app.use("/", indexRouter);
  app.use("/api/administrator", administratorRouter);
  app.use("/api/roles", roleRouter);
  app.use("/api/subscribtions", subscribtionRouter);
  app.use("/api/privileges", privilegeRouter);
  app.use("/api/teachers", teachersRoute);
  app.use("/api/achats", achatsRoute);
  app.use("/api/classrooms", classRoomRoute);
  app.use("/api/students", studentsRoute);
  app.use("/api/course", courseRouter);
  app.use("/api/course", contentRouter);

  app.use(error);
};
