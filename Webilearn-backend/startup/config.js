/**
 *  Check JWT
 */
module.exports = function() {
  if (!process.env.JWT) {
    throw new Error("FATAL ERROR: jwtPrivateKey is not defined.");
  }
};
