const winston = require("winston");
const mongoose = require("mongoose");

/**
 *  Database Connection
 */
module.exports = function(app) {
  mongoose.set('useCreateIndex', true);
  mongoose
    .connect("mongodb://localhost/Webilearn", {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex:true
})
    .then(() => winston.info("DB connected"))
    .catch(err => console.error("Error DB not connected!"));
    
  };
