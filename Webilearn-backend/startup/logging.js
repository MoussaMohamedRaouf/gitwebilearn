require("winston-mongodb");
const winston = require("winston");
/**
 *  Trace errors in logfile
 */
module.exports = function() {
  winston.add(
    new winston.transports.File({
      filename: "./logs/logfile.log",
      handleExceptions: true
    })
  );
  winston.add(
    new winston.transports.MongoDB({
      db: "mongodb://localhost/vidly",
      level: "error"
    })
  );
  winston.add(
    new winston.transports.Console({ Colorize: true, prettyPrint: true })
  );
};
