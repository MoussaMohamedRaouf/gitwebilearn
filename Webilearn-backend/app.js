const env = require('dotenv');
env.config();
var express = require('express');
var app = express();
var cors = require('cors');
app.use(cors());

var cookieParser = require('cookie-parser');
var logger = require('morgan');
var createError = require('http-errors');
var path = require('path');





require('./startup/routes')(app);
require('./startup/validation')(app,express);
require('./startup/db')();
require('./startup/logging')();
require('./startup/config')();

var router = express.Router();



module.exports = function(app,express){
  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'ejs');

  app.use(logger('dev'));
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));
  
  
  
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

}


module.exports = app;
