const { Classroom, validateClassroom } = require('../models/classroom');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId } = require('../shared/validator');
const { Student } = require('../models/student');

const _ = require('lodash');

exports.getAllClassrooms = function (req, res) {
    Classroom.find((err, classRooms) => {
        if (err) { return errorSending(res, err, 'Looking for classRooms failed.'); }
        dataSending(res, classRooms);
    }).populate('students').populate('tuteur');
}

/**
 * Adding a privilege to Database
 */
exports.newClassroom = function (req, res) {
    let params = checkParams(req);
    let newclassroom = new Classroom(_.pick(params, ['titre', 'description', 'tuteur', 'date', 'heureDebut', 'dureEnHeures','dureEnMinutes']));
    classroomId = newclassroom._id;
  
    newclassroom.save();
    addStudentsToCourse(params,newclassroom._id);
    dataSending(res, newclassroom);
}



/**
 * get one privileges from database
 */
exports.getClassroom = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.', error); }
    Classroom.findById(req.params.id, (err, classRoom) => {
        if (!classRoom) return errorSending(res, 'Error.', 'No such classRoom found on database.');
        return dataSending(res, classRoom);
    }).populate('students').populate('tuteur');
}


/**
 * Update a privilege  a role from database
 */
exports.updateClassroom = function (req, res) {
    let params = checkParams(req);
    Classroom.findOneAndUpdate({_id:req.params.id}, params, { new: true }, (err, classroom) => {
        if (err) { return errorSending(res, err, 'Internal server Error.'); }
        classroom.students = undefined;
        classroom.save();
        addStudentsToCourse(params,req.params.id);
        successSending(res,'Done','Classroom updated successfully.');
    });

}
/**
 * Deleting a privilege from database
 */
exports.deleteClassroom = function (req, res) {

    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.', error); }

    Classroom.findByIdAndDelete(req.params.id, (err) => {
        if (err) return errorSending(res, 'Error.', 'No such classroom found on database.');
        successSending(res, 'Done.', 'Classroom deleted successfully.');
    });
}

function addStudentsToCourse(params,classroomId) {
    console.log(params.students);
    params.students.forEach(id => {
        Student.findById(id, (err, student) => {
            Classroom.updateOne({ _id: classroomId }, { $push: { students: student } }, (err, result) => {
            });
        });
    });
}

function checkParams(req) {
    let params = {
        titre: req.body.titre,
        tuteur: req.body.tuteur,
        description: req.body.description,
        date: req.body.date,
        students: req.body.students,
        heureDebut: req.body.heureDebut,
        dureEnHeures: req.body.dureEnHeures,
        dureEnMinutes: req.body.dureEnMinutes
    };
    for (let prop in params)
        if (!params[prop])
            delete params[prop];
    return params;
}
