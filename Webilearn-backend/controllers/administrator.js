const {
  Administrator,
  validateSignIn,
  validateAdmin,
  validatePassword,
  validateAdminUpdate,
  validatePasswordModification,
  hasExpiredorInvalid,
  getDataFromToken
} = require('../models/administrator');
const { Role } = require('../models/role');
const { validateId } = require('../shared/validator');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { sendMail } = require('../shared/mails');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const attributesNotToRender = '-password -resetPasswordToken -updatedAt -__v';
const faker = require('faker');
const FakerAdminsPassword = "Lapakita123'";
const SuperAdminId = '5e970e6463262948a4d4fece';

/**
 * Adding random data to database with faker
 */
exports.fakerAdminSignUp = function (req, res) {
  if (!req.body.x) { errorSending(res, 'Error', 'X parameter not provided.') }
  for (let index = 0; index < req.body.x; index++) {
    administrator = new Administrator();
    administrator.userName = faker.internet.userName();
    administrator.email = faker.internet.email();
    administrator.bio = faker.lorem.paragraph();
    administrator.firstName = faker.name.firstName();
    administrator.lastName = faker.name.lastName();
    administrator.password = FakerAdminsPassword;
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(administrator.password, salt, function (err, hash) {
        administrator.password = hash;
        administrator.save();
      });
    });
  }
  successSending(res, req.body.x, "Admins created.")

}
/**
 * Sign up function
 */
exports.createNewAdministrator = function (req, res) {
  const passwordPattern = /^(?=.*[A-Z])(?=.*[\W])(?=.*[0-9])(?=.*[a-z]).{8,30}$/;
  const passwordPrefix = "Bc'94"
  Administrator.findOne({ email: req.body.email }, (err, administrator) => {
    if (administrator) return errorSending(res, 'Error.', 'This email is already registered.');
    administrator = new Administrator(_.pick(req.body, ['userName', 'email']));
    administrator.password = faker.internet.password(10, 16, passwordPattern, passwordPrefix);
    administrator.firstConnection = true;
    sendMail(administrator, res, "new");
    administrator = adminCreation(administrator, req, res);
  });
}
/**
 * Sign up function
 */
exports.adminSignUp = function (req, res) {
  const { error } = validateAdmin(req.body);
  if (error) { return errorSending(res, 'Validation error.', error); }
  Administrator.findOne({ email: req.body.email }, (err, administrator) => {
    if (administrator) return errorSending(res, 'Error.', 'This email is already registered.');
    administrator = new Administrator(_.pick(req.body, ['userName', 'email', 'password', 'bio']));
    administrator = adminCreation(administrator, req, res);
  });
}
/**
 * Sign in function
 */
exports.adminSignIn = function (req, res) {
  const { error } = validateSignIn(req.body);
  if (error) {
    return errorSending(res, 'Error.', error.details[0].message);
  }
  Administrator.findOne({ email: req.body.email }, function (err, administrator) {
    if (!administrator) return errorSending(res, 'Error', 'Invalid email.');
    administrator = adminConnection(req, administrator, res);
  });
}
/** Get Administrator /me  */
exports.getAdmin = function (req, res) {
  Administrator.findById(req.user._id, function (err, administrator) {
    res.send(administrator);
  }).select(attributesNotToRender).populate('role');
}

/**
* Get Administrators from database
*/
exports.getAdministrators = function (req, res) {
  Administrator.find({ _id: { $ne: SuperAdminId } },(err, admins) => {
    if (err) { return errorSending(res, err, 'Looking for administrator failed.'); }
    dataSending(res, admins);
  }).select(attributesNotToRender).populate('role');
}


/**
 * Recover password function
 */
exports.recover = (req, res) => {
  Administrator.findOne({ email: req.body.email })
    .then(user => {
      if (!user) return errorSending(res, 'Server', 'The email address ' + req.body.email + ' is not associated with any account. Double-check your email address and try again.');
      if (!hasExpiredorInvalid(user.resetPasswordToken)) return errorSending(res, 'Server', 'Please visit' + req.body.email + 'and click the link to reset your password.');
      const token = user.generatePassToken();
      user.save()
        .then(user => {
          sendMail(user, res, "recover");
        }).catch(err => errorSending(res, err.message, 'Error.'));
    }).catch(err => errorSending(res, err.message, 'Error.'));
};

/**
 * Update Administrator password function
 */
exports.updateAdministratorPassword = (req, res) => {
  Administrator.findById(req.user._id, (err, user) => {
    if (err) return errorSending(res, 'Error.', 'No such user found on servers.');
    const { error } = validatePasswordModification(req.body);
    if (error) return errorSending(res, 'Error servers.', error.details[0].message);
    if (req.body.newPassword !== req.body.confirmedPassword) return errorSending(res, 'Error', 'New password and confirmation dosent match.');
    bcrypt.compare(req.body.password, user.password, function (err, result) {
      if (!result) return errorSending(res, 'Error', 'Wrong password provided.');
      bcrypt.hash(req.body.newPassword, 10, function (err, newHash) {
        user.password = newHash;
        user.save((err) => {
          if (err) {
            return errorSending(res, 'Server:', err);
          }
          successSending(res, 'Done.', 'Password updated successfully.');
        });
      });
    });
  });

}
/**
 * Reset password function
 */
exports.resetPassword = (req, res) => {
  Administrator.findOne({ resetPasswordToken: req.params.token }, function (err, user) {
    if (err) return errorSending(res, 'Error', 'Invalid token provided');
    if (hasExpiredorInvalid(req.params.token)) return errorSending(res, 'Invalid.', 'Token Provided has expired use home page to generate a new one.');

    const { error } = validatePassword(req.body);
    if (error) { return errorSending(res, error.details[0].message, 'Error servers.'); }
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(req.body.newPassword, salt, function (err, hash) {
        user.password = hash;
        user.resetPasswordToken = undefined;
        resetPasswordSaving(user, res);
      });
    });
  });
}
/**
 * verify users token function
 */
exports.validateSessionToken = (req, res) => {
  const token = req.body.token;
  if (!token) return errorSending(res, 'Error.', 'Token required.');
  if (hasExpiredorInvalid(token)) return errorSending(res, 'Error.', 'Session expired.');
  payload = getDataFromToken(token);
  if (!payload._id) return errorSending(res, 'Error.', 'Session expired please login again use home page to generate a new one.');
  Administrator.findById(payload._id, (err, admin) => {
    if (!admin) return errorSending(res, 'Error', 'Session expired please login again use home page to generate a new one.');
    dataSending(res, payload);
  });
}

/**
 * Validate password token for post process
 */
exports.valdiatePasswordToken = (req, res) => {
  Administrator.findOne({ resetPasswordToken: req.body.token }, function (err, admin) {
    if (!admin) return errorSending(res, 'Invalid.', 'Token Provided is invalid.');
    if (hasExpiredorInvalid(req.body.token)) return errorSending(res, 'Invalid.', 'Token Provided has expired.');
    return successSending(res, 'Valid token provided.', 'Found');
  });
}

/**
 * Saving the user post verification
 * @param {*} user 
 * @param {*} hash 
 * @param {*} res 
 */
function resetPasswordSaving(user, res) {
  user.save((err) => {
    if (err) {
      return errorSending(res, err, 'Error validation.');
    }
    sendMail(user, res, "confirmation");
    dataSending(res, { user: user });
  });
}



/**
 * admin attempt connextion
 * @param {*} req 
 * @param {*} administrator 
 * @param {*} res 
 */
function adminConnection(req, administrator, res) {
  bcrypt.compare(req.body.password, administrator.password, function (err, result) {
    if (!result) {errorSending(res, 'Error', 'Invalid email or password.');}
    else if(!administrator.status) {errorSending(res, 'Error','Account desactivated, please contact your administrator.');}
    else{const token = administrator.generateAuthToken();
      dataSending(res, token);
    }    
  });
}
/**
 * Admin Initiatiation
 * @param {*} administrator 
 * @param {*} req 
 * @param {*} res 
 */
function adminCreation(administrator, req, res) {
  bcrypt.genSalt(10, function (err, salt) {
    bcrypt.hash(administrator.password, salt, function (err, hash) {
      administrator.password = hash;
      saveAdministrator(administrator, res);
    });
  });
  return administrator;
}
/**
 * Save administrator
 * @param {Administrator} administrator 
 * @param {string} hash 
 * @param {Response} res 
 */
function saveAdministrator(administrator, res) {
  administrator.save((err) => {
    if (err) { return errorSending(res, err, 'Validation error.'); }
    successSending(res, "Succes", "Admin created.")
  });
}


/**
* Deleting a Administrator from database
*/
exports.deleteAdministrator = function (req, res) {
  const { error } = validateId(req.body);
  if (error) { return errorSending(res, error, 'Incorrect informations error.'); }
  Administrator.findById(req.body.id, (err, admin) => {
    if (!admin) return errorSending(res, 'Error', 'No such admin found on database.');
    admin.delete((err) => {
      if (err) { return errorSending(res, err, 'Deleting admin failed.'); }
      else { successSending(res, true, 'Administrator deleted successfully.', 'Found') }
    });
  });
}
/**
* Update a Administrator in database
*/
exports.firstSignInAdministrator = function (req, res) {
  const { errorBody } = validateAdminUpdate(req.body);
  if (errorBody) { return errorSending(res, 'Server:', errorBody); }
  console.log(req.file);
  let params = {
    userName: req.body.userName,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    bio: req.body.bio,
    profileImage: req.file.filename,
    adress:req.body.streetName+','+req.body.streetNumber+','+req.body.city,
  };
  for (let prop in params) if (!params[prop]) delete params[prop];
  Administrator.findByIdAndUpdate(req.user._id, params, { new: true }, (err, admin) => {
    if (err) { return errorSending(res, err, 'Internal server Error.'); }
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(req.body.newPassword, salt, function (err, hash) {
        admin.password = hash;
        admin.firstConnection = undefined;
        admin.save(req.user._id);
        const token = admin.generateAuthToken();
        dataSending(res, token);
      });
    });
  });
}
/**
* Update a Administrator in database
*/
exports.updateAdministrator = function (req, res) {
  const { errorBody } = validateAdminUpdate(req.body);
  if (errorBody) { return errorSending(res, 'Server:', errorBody); }
  let params = {
    userName: req.body.userName,
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    bio: req.body.bio,
  };
  for (let prop in params) if (!params[prop]) delete params[prop];
  Administrator.findByIdAndUpdate(req.user._id, params, { new: true }, (err, admin) => {
    if (err) { return errorSending(res, err, 'Internal server Error.'); }
    successSending(res, 'Done.', 'Informations were updated successfully.')
  });
}


/**
* Add Role to an Administrator
*/
exports.addRoleToAdministrator = function (req, res) {

  const { err } = validateId(req.params);
  if (err) { return errorSending(res, err, 'Incorrect role ID.'); }
  
  Administrator.findById(req.params.id, (err, admin) => {
    if (!admin) return errorSending(res, 'Error', 'No such admin found on database.');
    Role.findById(req.body.roleId, (err, role) => {
      if (!role) return errorSending(res, 'Error', 'No such role found on database.');
      admin.role = role;
      admin.save(function (err) {
        if (err) return errorSending(res, 'Error', 'Cannot add this role to this admin.');
        successSending(res,'Server','Role updated successfully');
      });
    });
  });
}

/**
* Delete Role to an Administrator
*/
exports.deleteRoleToAdministrator = function (req, res) {
  const { error } = validateId(req.params);
  if (error) { return errorSending(res, error, 'Incorrect informations error.'); }
  const { errorRole } = validateId(req.body);
  if (errorRole) { return errorSending(res, errorRole, 'Incorrect informations error.'); }

  Administrator.findById(req.params.id, (err, admin) => {
    if (!admin) return errorSending(res, 'Error', 'No such admin found on database.');
    Role.findById(req.body.id, (err, role) => {
      if (!role) return errorSending(res, 'Error', 'No such role found on database.');
      admin.role = undefined;
      admin.save(function (err) {
        if (err) return errorSending(res, 'Error', 'Cannot add this role to this admin.');
        return dataSending(res, admin);
      });
    });
  });
}
/**
* Delete Role to an Administrator
*/
exports.changeStatus = function (req, res) {
  const { error } = validateId(req.body);
  if (error) { return errorSending(res, error, 'Incorrect informations error.'); }
  Administrator.findById(req.body.id, (err, admin) => {
    if (!admin) return errorSending(res, 'Error', 'No such admin found on database.');
      admin.status =!admin.status;
      admin.save(function (err) {
        if (err) return errorSending(res, 'Error', 'Cannot add this role to this admin.');
        return dataSending(res,admin);
      });
    });
}





