const { Content } = require('../models/content');
const { Cours } = require('../models/cours');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId , validateContentId } = require('../shared/validator');
const storing = require("../shared/storing");



exports.getOneCoursContents = function (req, res) {
    const { paramError } = validateId(req.params);
    if (paramError) { return errorSending(res, 'Server:', paramError); }
    
    Cours.findById(req.params.id,(err,cours) => {
        if (err) { return errorSending(res, err, 'Looking for courses failed.'); }
        dataSending(res,cours);
    }).populate('contents');

}


exports.getOneCoursOneContent = function (req, res) {
    const { idError } = validateId(req.params);
    if (idError) { return errorSending(res, 'Server:', idError); }
    
    Cours.find({ _id:req.params.id, contents: { $elemMatch:  { "_id" : req.params.contentId } } },(err,course)=>{
        if (err) { return errorSending(res, 'Server:', err); }
        Content.findById(req.params.contentId,(err,content)=>{
            if (err) { return errorSending(res, 'Server:', err); }
            dataSending(res,content);
        })    
    }).populate('contents');
}

/**
 * create new cours
 */
exports.newCoursContent = function (req, res) {
    if(!req.file){
        errorSending(res,'Error','Fichier obilgatoire.');
    }
    const { idError } = validateId(req.params);
    if (idError) { return errorSending(res, 'Server:', idError); }
    generateContent(req);
    console.log(content);
    Cours.findOneAndUpdate({_id:req.params.id},
        {$push: {contents: content}},
        (err,course)=>{
            if(err) return errorSending(res, 'Server:', err);
            successSending(res,'server','Content created successfully.');
        }).populate('contents');
}
/**
 * Update a Content
 */
exports.updateCoursContent = function (req, res) {
    const { idError } = validateId(req.params);
    if (idError) { return errorSending(res, 'Server:', idError); }
    const { contentError } = validateContentId(req.params);
    if (contentError) { return errorSending(res, 'Server:', contentError); }
   
    Cours.findById(req.params.id, (err, cours) => {
        if (err) { return errorSending(res, 'Server:', err); }
        Content.findById(req.params.contentId,(err,content)=>{
            if (err) { return errorSending(res, 'Server:', err); }
            storing.deleteFile('./uploads/courseContents',content.location);
            content.title = req.body.title;
            content.description = req.body.description;
            content.location = req.file.filename;
            content.save();
            successSending(res,'success','File updated successfully');
        });
    }).populate('contents');
}
/**
 * Deleting a privilege from database
 */
exports.deleteCoursContent = function (req, res) {

    const { idError } = validateId(req.params);
    if (idError) { return errorSending(res, 'Server:', idError); }
    const { contentError } = validateContentId(req.params);
    if (contentError) { return errorSending(res, 'Server:', contentError); }

    Cours.findById(req.params.id,(err,cours) => {
        if (err) { return errorSending(res, err, 'Looking for course failed.'); }
        Content.findById(req.params.contentId,(err,content)=>{
            if (err) { return errorSending(res, err, 'Looking for content failed.'); }
            storing.deleteFile('./uploads/courseContents',content.location,res);
        });
        Content.findByIdAndDelete(req.params.contentId);  
        cours.contents.remove(req.params.contentId);
        cours.save((err,cours)=>{
            if (err) { return errorSending(res, err, 'Deleteing Content failed.'); }
            dataSending(res,cours)
        });
    }).populate('contents');
}


function generateContent(req) {
    content = new Content();
    content.title = req.body.title;
    content.description = req.body.description;
    content.location = req.file.filename;
    content.mimetype = req.file.mimetype;
    content.save();
}

