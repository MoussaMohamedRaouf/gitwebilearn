const { Role, validateRole, validateRoleName } = require('../models/role');
const { Privilege } = require('../models/privilege');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const _ = require('lodash');
const { validateId } = require('../shared/validator');
const { Administrator } = require('../models/administrator');

const administratorRoleId = '5e922fb4272ac11af8111b53';

/**
 * Adding a role to Database
 */
exports.addRole = function (req, res) {
  const { error } = validateRoleName(req.body);
  if (error) { return errorSending(res, 'Error.', error); }
  role = new Role(_.pick(req.body, ['name']));
  role.save((err) => {
    if (err) { return errorSending(res, err, 'Saving data error.'); }
    else { successSending(res, 'Done', 'Role created successfully.') }
  });
}

/**
 * Deleting a role from database
 */
exports.deleteRole = function (req, res) {
  const { error } = validateId(req.body);
  if (error) { return errorSending(res, 'Error.', error); }
  Role.findById(req.body.id, (err, role) => {
    if (!role) return errorSending(res, 'Error.', 'No such role found on database.');
    role.delete((err) => {
      if (err) { return errorSending(res, err, 'Deleting role failed.'); }
      successSending(res, 'Done.', 'Role deleted successfully.');
    });
  });
}

/**
 * Update a role  a role from database
 */
exports.updateRole = function (req, res) {
  const { error } = validateRole(req.body);
  if (error) return errorSending(res, 'Error', error);
  Role.findById(req.body.id, (err, role) => {
    if (!role) return errorSending(res, 'Error', 'No such role found on database.');
    role.name = req.body.name;
    role.save((err) => {
      if (err) return errorSending(res, 'Error', err);
      successSending(res, 'Done.', 'Role updated successfully.');
    });
  });
}

/**
 * Get roles from database
 */
exports.getRoles = function (req, res) {
  Role.find((err, roles) => {
    if (err) { return errorSending(res, 'Error.', err); }
    dataSending(res, roles);
  });
}
/**
 * Get roles from database
 */
exports.getRolesExceptMine = function (req, res) {
  const { error } = validateId(req.params);
  if (error) { return errorSending(res, 'Error.', error); }

    Administrator.findById(req.params.id, (err, admin) => {
      if (err) { return errorSending(res, 'Error.', err); }
      Role.find({ _id: { $nin: [admin.role,administratorRoleId] } }, (err, roles) => {
        if (err) { return errorSending(res, 'Error.', err); }
        dataSending(res, roles);
      });
  
    });

}
/**
 * get one role from database
 */
exports.getRole = function (req, res) {
  const { error } = validateId(req.params);
  if (error) { return errorSending(res, 'Error.', error); }
  Role.findById(req.params.id, (err, role) => {
    if (!role) return errorSending(res, 'Error.', 'No such role found on database.');
    dataSending(res, role);
  });
}


/**
* Add Role to an Administrator
*/
exports.addPrivilegeToRole = function (req, res) {
  const { error } = validateId(req.params);
  if (error) { return errorSending(res, 'Error.', error); }
  const { errorRole } = validateId(req.body);
  if (errorRole) { return errorSending(res, 'Error.', errorRole); }

  Role.findById(req.params.id, (err, role) => {
    if (!role) return errorSending(res, 'Error.', 'No such role found on database.');
    Privilege.findById(req.body.id, (err, privilege) => {
      if (!privilege) return errorSending(res, 'Error.', 'No such privilege found on database.');
      role.privileges.push(privilege);
      role.save(function (err) {
        if (err) return errorSending(res, 'Error.', 'Cannot add this privilege to this role.');
        successSending(res, 'Done.', 'Privilege added to role successfully.');
      });
    });
  });
}

/**
* delete Privilege to an Role
*/
exports.deletePrivilegeToRole = function (req, res) {
  const { error } = validateId(req.params);
  if (error) { return errorSending(res, 'Error.', error); }
  const { errorRole } = validateId(req.body);
  if (errorRole) { return errorSending(res, 'Error.', errorRole); }

  Role.findById(req.params.id, (err, role) => {
    if (!role) return errorSending(res, 'Error.', 'No such role found on database.');
    Privilege.findById(req.body.id, (err, privilege) => {
      if (!privilege) return errorSending(res, 'Error.', 'No such privilege found on database.');
      role.privileges.remove(privilege);
      role.save(function (err) {
        if (err) return errorSending(res, 'Error.', 'Cannot add this privilege to this role.');
        successSending(res, 'Done.', 'Privilege deleted from role successfully.');
      });
    });
  });
}

