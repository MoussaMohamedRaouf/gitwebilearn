const { Student, validateStudent } = require('../models/student');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId } = require('../shared/validator');
const _ = require('lodash');
const faker = require('faker');



function studentList(res,patricipants) {
    patricipantsTab= [];
    console.log(patricipants);
    patricipants.forEach(id => {
        const { error } = validateId(id);
        if (error) { return errorSending(res, 'Error.',  error);}
            Student.findById(id,(err,etudiant)=>{
                if (err) { return errorSending(res, 'Error.',  err);}
                patricipantsTab.push(etudiant);
            });
    });
    return patricipantsTab;
  }

/**
 * Student change status function
 */
    exports.changeStatus = function(req,res){
    const { errorId } = validateId(req.params);
    if (errorId) { return errorSending(res, 'Error.',  errorId);}

    Student.findById(req.params.id,(err,student)=>{
        student.status = !student.status;
        student.save((err)=>{
            if (err) { return errorSending(res, 'Error.',  err);}
            successSending(res,'Server','Student status updated');
        })
    });
    }



/**
 * Adding random students to database with faker
 */
exports.fakerStudentsCreation = function (req, res) {
    if (!req.body.x) { errorSending(res, 'Error', 'X parameter not provided.') }
    for (let index = 0; index < req.body.x; index++) {
      student = new Student();
      student.userName = faker.internet.userName();
      student.email = faker.internet.email();
      student.save();
    }
    successSending(res, req.body.x, "Students were created.")
  
  }







/**
 * getStudents from database
 */
exports.getStudents = function (req,res) {
    Student.find((err,students) => {
        if (err) { return errorSending(res, err, 'Looking for students failed.'); }
        dataSending(res,students);
    });
}
/**
 * getStudents from database
 */
exports.getStudentsIds = function (req,res) {
    Student.find((err,students) => {
        if (err) { return errorSending(res, err, 'Looking for students failed.'); }
        dataSending(res,students);
    }).select('email');
}

/**
 * newStudent to Database
 */
exports.newStudent = function (req, res) {
    const { error } = validateStudent(req.body);
    if (error) { return errorSending(res, 'Error.',  error);}
    student = new Student(_.pick(req.body, ['userName','email']));
    student.save((err) => {
        if (err) { return errorSending(res, 'Error.',  err); }
        else { successSending(res, 'Done.', 'Student created successfully'); 
    }
});
  }
  

/**
 * getOneStudent from database
 */
exports.getOneStudent = function (req,res) {

    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Student.findById(req.params.id, (err,student) => {
        if (err) return errorSending(res, 'Error.',  'No student found on database.');
        return dataSending(res,student);
    });
}


/**
 * updateStudent in database
 */
exports.updateStudent = function (req, res) {
    const { errorId } = validateId(req.params);
    if (errorId) { return errorSending(res, 'Error.',  errorId);}
    const { errorBody } = validateStudent(req.body);
    if (errorBody) { return errorSending(res, 'Error.',  errorBody);}
    
    Student.findById(req.params.id, (err,student) => {
        if (!student) return errorSending(res,  'Error.', 'No student found on database.');
        student.userName=req.body.userName;
        student.email=req.body.email;
        student.save((err)=>{
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Student updated successfully.') }
        });
    });
}
/**
 * deleteStudent from database
 */
exports.deleteStudent = function (req, res) {

    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Student.findByIdAndDelete(req.params.id, (err,student) => {
        if (!student) return errorSending(res, 'Error.',  'No such student found on database.');
         successSending(res, 'Done.', 'Student deleted successfully.') 
    });
}

exports.studentList = studentList;