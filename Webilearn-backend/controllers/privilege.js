const { Privilege, validatePrivilege } = require('../models/privilege');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId } = require('../shared/validator');
const _ = require('lodash');

exports.getAllPrivileges = function (req,res) {
    Privilege.find((err,privileges) => {
        if (err) { return errorSending(res, err, 'Looking for roles failed.'); }
        dataSending(res,privileges);
    });
}


/**
 * Adding a privilege to Database
 */
exports.newPrivilege = function (req, res) {
    const { error } = validatePrivilege(req.body);
    if (error) { return errorSending(res, 'Error.',  error);}
    privilege = new Privilege(_.pick(req.body, ['name','description']));
    privilege.save((err) => {
        if (err) { return errorSending(res, 'Error.',  err); }
        else { successSending(res, 'Done.', 'Privilege created successfully'); 
    }
      });
  }
  

/**
 * get one privileges from database
 */
exports.getPrivilege = function (req,res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Privilege.findById(req.params.id, (err,privilege) => {
        if (!privilege) return errorSending(res, 'Error.',  'No such privilege found on database.');
        return dataSending(res,privilege);
    });
}


/**
 * Update a privilege  a role from database
 */
exports.updatePrivilege = function (req, res) {
    const { errorId } = validateId(req.params);
    if (errorId) { return errorSending(res, 'Error.',  errorId);}
    
    const { errorPriv } = validatePrivilege(req.body);
    if (errorPriv) { return errorSending(res, 'Error.',  errorPriv);}
   
    Privilege.findById(req.params.id, (err,privilege) => {
        if (!privilege) return errorSending(res,  'Error.', 'No such privilege found on database.');
        privilege.name=req.body.name;
        privilege.description=req.body.description;
        privilege.save((err)=>{
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Privilege updated successfully.') }
        });
    });
}
/**
 * Deleting a privilege from database
 */
exports.deletePrivilege = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Privilege.findById(req.params.id, (err,privilege) => {
        if (!privilege) return errorSending(res, 'Error.',  'No such privilege found on database.');
        privilege.delete( (err) => {
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Privilege deleted successfully.') }
        });
    });
}