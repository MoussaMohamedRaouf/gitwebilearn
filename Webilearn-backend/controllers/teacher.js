const { Teacher, validateTeacher } = require('../models/teacher');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId } = require('../shared/validator');
const _ = require('lodash');
const faker = require('faker');

/**
 * Teacher change status function
 */
exports.changeStatus = function(req,res){
    const { errorId } = validateId(req.params);
    if (errorId) { return errorSending(res, 'Error.',  errorId);}

    Teacher.findById(req.params.id,(err,teacher)=>{
        if(err) { return errorSending(res, 'Error.',  err);}
        teacher.status = !teacher.status;
        teacher.save((err)=>{
            if (err) { return errorSending(res, 'Error.',  err);}
            successSending(res,'Server','Teacher status updated');
        });
    });
    }


/**
 * Adding random students to database with faker
 */
exports.fakerTeachersCreation = function (req, res) {
    if (!req.body.x) { errorSending(res, 'Error', 'X parameter not provided.') }
    for (let index = 0; index < req.body.x; index++) {
      teacher = new Teacher();
      teacher.userName = faker.internet.userName();
      teacher.email = faker.internet.email();
      teacher.bio = faker.lorem.paragraph();
      teacher.save();
    }
    successSending(res, req.body.x, "Teachers were created.")
  
  }

exports.getTeachers = function (req, res) {
    Teacher.find((err, teachers) => {
        if (err) { return errorSending(res, err, 'Looking for teacgers failed.'); }
        dataSending(res, teachers);
    });
}

exports.getTeachersIds = function (req, res) {
    Teacher.find((err, teachers) => {
        if (err) { return errorSending(res, err, 'Looking for teacgers failed.'); }
        dataSending(res, teachers);
    }).select('email');
}


/**
 * Adding a privilege to Database
 */
exports.newTeacher = function (req, res) {
    const { error } = validateTeacher(req.body);
    if (error) { return errorSending(res, 'Error.', error); }
    teacher = new Teacher(_.pick(req.body, ['userName', 'email','bio']));
    teacher.save((err) => {
        if (err) { return errorSending(res, 'Error.', err); }
        else {
            successSending(res, 'Done.', 'Teacher created successfully');
        }
    });
}



/**
 * get one privileges from database
 */
exports.getOneTeacher = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Teacher.findById(req.params.id, (err,teacher) => {
        if (err) return errorSending(res, 'Error.',  'No teacher found on database.');
        return dataSending(res,teacher);
    });
}


/**
 * Update a privilege  a role from database
 */
exports.updateTeacher = function (req, res) {

    const { errorId } = validateId(req.params);
    if (errorId) { return errorSending(res, 'Error.',  errorId);}

    const { errorPriv } = validateTeacher(req.body);
    if (errorPriv) { return errorSending(res, 'Error.',  errorPriv);}

    Teacher.findById(req.params.id, (err,teacher) => {
        if (err) return errorSending(res,  'Error.', 'No teacher found on database.');
        teacher.useName=req.body.userName;
        teacher.email=req.body.email;
        teacher.bio=req.body.bio;
        teacher.save((err)=>{
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Teacher updated successfully.') }
        });
    });
}
/**
 * Deleting a privilege from database
 */
exports.deleteTeacher = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Teacher.findById(req.params.id, (err,teacher) => {
        if (!teacher) return errorSending(res, 'Error.',  'No teacher found on database.');
        teacher.delete( (err) => {
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Teacher deleted successfully.') }
        });
    });
}
