const { Achat, achatSchema } = require('../models/achat');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId } = require('../shared/validator');
const { Student } = require('../models/student');
const { Cours } = require('../models/cours');

const _ = require('lodash');

exports.getAllAchats = function (req, res) {
    Achat.find((err, achats) => {
        if (err) { return errorSending(res, err, 'Erreur.'); }
        dataSending(res, achats);
    }).populate('student').populate('course');
}

exports.getOneAchat = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.', error); }
    Achat.findById(req.params.id, (err, achat) => {
        if (!achat) return errorSending(res, 'Error.', 'Pas dachat trouvé avec cet idantifiant.');
        return dataSending(res, achat);
    }).populate('student').populate('course');
}
/**
 * Adding achat to Database
 */
exports.newAchat = function (req, res) {

    Student.findById(req.body.student, (err, student) => {
        if (err) return errorSending(res, 'Error.', 'Pas detudiant trouvé avec cet idantifiant.');
        Cours.findById(req.body.course, (err, course) => {
            if (err) return errorSending(res, 'Error.', 'Pas de cours trouvé avec cet idantifiant.');
            newAchat = new Achat();
            newAchat.student = student;
            newAchat.course = course;
            console.log(newAchat);
            newAchat.save();
            successSending(res, 'Done', 'Achat realiser avec success.');
        });
    });
}

/**
 * Update achat  from database
 */
exports.updateAchat = function (req, res) {
    Student.findById(req.body.studentId, (err, student) => {
        if (err) return errorSending(res, 'Error.', 'Pas detudiant trouvé avec cet idantifiant.');
        Cours.findById(req.body.courseId, (err, course) => {
            if (err) return errorSending(res, 'Error.', 'Pas de cours trouvé avec cet idantifiant.');
            Achat.findByIdAndUpdate(req.params.id, { student: student, course: course }, (err, achat) => {
                if (err) return errorSending(res, 'Error.', 'Action non réaliser.');
                successSending(res, 'Done', 'Achat mis a jour avec success.');
            });
        });
    });
}
/**
 * Deleting achat from database
 */
exports.deleteAchat = function (req, res) {
    Achat.findByIdAndDelete(req.params.id, (err, achat) => {
        if (err) return errorSending(res, 'Error.', 'Pas dachat trouvé avec cet idantifiant.');

        successSending(res, 'Done', 'Achat supprimer avec success.');
    });
}