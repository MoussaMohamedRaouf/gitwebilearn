const { successSending, errorSending, dataSending } = require('../shared/alert');
const { Cours, validateCours } = require('../models/cours');
const { Teacher } = require('../models/teacher');
const { Content } = require('../models/content');
const faker = require('faker');
const _ = require('lodash');
const { validateId } = require('../shared/validator');
const storing = require("../shared/storing");


/**
 * Adding random courses to database with faker
 */
exports.fakerCoursesCreation = function (req, res) {
    if (!req.body.x) { errorSending(res, 'Error', 'X parameter not provided.') }
    for (let index = 0; index < req.body.x; index++) {
        cours = new Cours();
        cours.name = faker.name.firstName();
        cours.description = faker.lorem.paragraph();
        cours.save();
    }
    successSending(res, req.body.x, "Courses were created.")

}

/**
 * get all courses
 */
exports.getAllcourses = function (req, res) {
    Cours.find((err, courses) => {
        if (err) { return errorSending(res, 'Error', 'Looking for courses failed.'); }
        dataSending(res, courses);
    }).populate('contents').populate('teacher');
}
exports.getAllcoursesIds = function (req, res) {
    Cours.find((err, courses) => {
        if (err) { return errorSending(res, 'Error', 'Looking for courses failed.'); }
        dataSending(res, courses);
    }).select('name');
}
/**
 * get one cours
 */
exports.getCours = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Validation error.', error); }
    Cours.findById(req.params.id, (err, cours) => {
        if (err) { return errorSending(res, "Error", err); }
        dataSending(res, cours);
    }).populate('contents').populate('teacher');
}
/**
 * create new cours
 */
exports.newCours = function (req, res) {
    const { error } = validateCours(req.body);
    if (error) { return errorSending(res, 'Validation error.', error); }
    Teacher.findById(req.body.teacher, (err, teacher) => {

        if (!teacher) { return errorSending(res, 'Error.', "Cant find a teacher with this id"); }
        cours = new Cours();
        cours.name = req.body.name;
        cours.description = req.body.description;
        cours.teacher = teacher;
        cours.save((err, course) => {
            if (err) { return errorSending(res, 'Error', err); }
            successSending(res, 'Done','Cours créer.');
        });
    });
}



/**
 * Update cours
 */
exports.updateCours = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Validation error.', error); }
    let params = {
        name: req.body.name,
        description: req.body.description
    };
    Teacher.findById(req.body.teacher,(err,teacher)=>{
        if (err) { return errorSending(res, err, 'Cant find any teacher with that id.'); }
        Cours.findByIdAndUpdate(req.params.id, params, { new: true }, (err, cours) => {
            if (err) { return errorSending(res, err, 'Cant update any cours with the given informations.'); }
            console.log(cours);
            cours.teacher = teacher;
            cours.save();
            successSending(res, 'Done','Cours mis a jours.');
        });
});

}

/**
 * delete cours
 */
exports.deleteCours = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Validation error.', error); }

    Cours.findByIdAndDelete(req.params.id, (err, cours) => {
        if (err) { return errorSending(res, 'Error', err); }
        cours.contents.forEach(content => {

            Content.findById(content._id, (err, res) => {
                if (err) return errorSending(res, err, 'Cant delete related content id.');
                storing.deleteFile('./uploads/courseContents', content.location,res);
                Content.findByIdAndDelete(content._id);
            });

        });
    }).populate('contents');
    successSending(res, 'Server', 'Course and related contents deleted correctly.')
}