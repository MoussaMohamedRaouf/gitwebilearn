const { Subscribtion, validateSubscribtion } = require('../models/subscription');
const { successSending, errorSending, dataSending } = require('../shared/alert');
const { validateId } = require('../shared/validator');
const _ = require('lodash');


exports.getSubscribtions = function (req, res) {
    Subscribtion.find((err,subscribtions) => {
        if (err) { return errorSending(res, err, 'Looking for subscribtions failed.'); }
 
        dataSending(res,subscribtions);
    }).sort({cost: -1});
}


/**
 * Adding a privilege to Database
 */
exports.newSubscribtion = function (req, res) {
    const { error } = validateSubscribtion(req.body);
    if (error) { return errorSending(res, 'Error.', error); }
    subscribtion = new Subscribtion(_.pick(req.body, ['name', 'description', 'cost', 'dureeAbonnement']));
    console.log(subscribtion);
    subscribtion.save((err) => {
        if (err) { return errorSending(res, 'Error.', err); }
        else {
            successSending(res, 'Done.', 'Subscribtion created successfully');
        }
    });
}


/**
 * get one privileges from database
 */
exports.getSubscribtion = function (req, res) {
    const currencyConverter = new CurrencyConverter('EUR', 'USD'); // Outputs live currency results

    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}
    Subscribtion.findById(req.params.id, (err,subscribtion) => {
        if (!subscribtion) return errorSending(res, 'Error.',  'No such subscribtion found on database.');

        return dataSending(res,subscribtion);
    });
}

/**
 * Update a privilege  a role from database
 */
exports.updateSubscribtion = function (req, res) {

    const { errorId } = validateId(req.params);
    if (errorId) { return errorSending(res, 'Error.',  errorId);}

    const { errorBody } = validateSubscribtion(req.body);
    if (errorBody) { return errorSending(res, 'Error.',  errorBody);}

    Subscribtion.findById(req.params.id, (err,subscribtion) => {
        if (!subscribtion) return errorSending(res,  'Error.', 'No such subscribtion found on database.');
        subscribtion.name=req.body.name;
        subscribtion.description=req.body.description;
        subscribtion.cost=req.body.cost;
        subscribtion.dureeAbonnement=req.body.dureeAbonnement;
        subscribtion.save((err)=>{
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Subscribtion updated successfully.') }
        });
    });
}

/**
 * Deleting a privilege from database
 */
exports.deleteSubscribtion = function (req, res) {
    const { error } = validateId(req.params);
    if (error) { return errorSending(res, 'Error.',  error);}

    Subscribtion.findById(req.params.id, (err,subscribtion) => {
        if (!subscribtion) return errorSending(res, 'Error.',  'No such subscribtion found on database.');
        subscribtion.delete((err)=>{
            if (err) { return errorSending(res, 'Error.',  err); }
            else { successSending(res, 'Done.', 'Subscribtion deleted successfully.') }
        });
    });
}