const { User, validateUserName } = require('../models/user');
const { succesSending, errorSending } = require('../shared/alert');
const _ = require('lodash');

/**
 * Adding a User to Database
 */
exports.addUser = function (req, res) {
    const { error } = validateUserName(req.body);
    if (error) { return errorSending(res, error, 'Incorrect informations error.'); }
     user = new User(_.pick(req.body, ['username']));
     user.save((err) => {
         if (err) { return errorSending(res, err, 'Saving user failed.'); }
         else { succesSending(res, _.pick(role, ['id', 'username'])) }
     });
}

/**
 * Deleting a User from database
 */
exports.deleteUser = function (req, res) {
    // const { error } = validateRoleId(req.body);
    // if (error) { return errorSending(res, error, 'Incorrect informations error.');}
    // Role.findById(req.body.id, (err,role) => {
    //     if (!role) return errorSending(res, 'No such role found on database.');
    //     role.delete((err)=>{
    //         if (err) { return errorSending(res, err, 'Deleting role failed.'); }
    //         else { succesSending(res,'Role deleted successfully.') }
    //     });
    // });
}

/**
 * Update a User in database
 */
exports.updateUser = function (req, res) {
    // const { error } = validateRole(req.body);
    // if (error) { return errorSending(res, error, 'Incorrect informations error.');}
    // Role.findById(req.body.id, (err,role) => {
    //     if (!role) return errorSending(res, 'No such role found on database.');
    //     role.name=req.body.name;
    //     role.save((err)=>{
    //         if (err) { return errorSending(res, err, 'Updating role failed.'); }
    //         else { succesSending(res,'Role updated successfully.') }
    //     });
    // });
}

/**
 * Get users from database
 */
exports.getUsers = function (req, res) {
    // Role.find((err,roles) => {
    //     if (err) { return errorSending(res, err, 'Looking for roles failed.'); }
    //     succesSending(res,roles);
    // });
}
