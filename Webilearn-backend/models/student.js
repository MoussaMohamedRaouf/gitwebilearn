const Joi = require("@hapi/joi");
const mongoose = require("mongoose");

var Schema = mongoose.Schema;
const studentSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      required: false,
      minLenght: 5,
      maxlength: 30
    },
    email: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 255,
      unique: true
    },
    status: {
      type: Boolean,
      default:true
    },
  },
  { timestamps: true }
);

const Student = new mongoose.model("Student", studentSchema);
/**
 * @desc Validate SignUp
 * @access Public
 */
function validateStudent(admin) {
  const schema = Joi.object({
    userName: Joi.string()
      .min(5)
      .max(30)
      .required(),
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
  });
  return schema.validate(admin);
}
exports.Student = Student;
exports.studentSchema = studentSchema;
exports.validateStudent = validateStudent;