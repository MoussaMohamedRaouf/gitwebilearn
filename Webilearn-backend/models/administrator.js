const Joi = require("@hapi/joi");
const mongoose = require("mongoose");
const passwordComplexity = require("joi-password-complexity");
const jwt = require("jsonwebtoken");
const fs = require('fs')

const complexityOptions = {
  min: 8,
  max: 250,
  lowerCase: 1,
  upperCase: 1,
  numeric: 1,
  symbol: 1
};
var Schema = mongoose.Schema;
const administratorSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      required: false,
      minLenght: 5,
      maxlength: 30
    },
    email: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 255,
      unique: true
    },
    password: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 1024
    },
    bio: {
      type: String,
      required: false,
      minLenght: 10,
      maxlength: 1024
    },
    company: {
      type: String,
      minLenght: 5,
      maxlength: 15,
      default: 'WebiLearn'
    },
    firstName: {
      type: String,
      minLenght: 5,
      maxlength: 15
    },
    lastName: {
      type: String,
      minLenght: 5,
      maxlength: 15
    },
    adress: {
      type: String,
      minLenght: 5,
      maxlength: 1024,
    },
    profileImage: {
      type: String,
      required: false,
      minLenght: 5,
      maxlength: 1024,
      default:"marc.jpg"
    },

    resetPasswordToken: {
      type: String,
      required: false,
      minLenght: 5,
      maxlength: 255
    },
    role: {
      type: Schema.Types.ObjectId,
      ref: 'Role'
    },
    firstConnection: {
      type: Boolean,
    },
    status: {
      type: Boolean,
      default:true
    },
  },
  { timestamps: true }
);

/**
 * @desc Generate Auth token
 * @access Public
 */
administratorSchema.methods.generateAuthToken = function () {
  const expiresIn = 60 * 60 * 24;// a day
  const data = {
    _id: this._id, 
    userName: this.userName, 
    email: this.email,
    profileImage:this.profileImage, 
    role: this.role, 
    firstConnection:this.firstConnection };
  const token = jwt.sign(data, process.env.JWT, { expiresIn: expiresIn });
  return token;
};

function hasExpiredorInvalid(token) {
  if(!token) return true;
  try {
    jwt.verify(token, process.env.JWT);
  } catch(err) {
    return true;
  }
  return false;
}
// function getDataFromToken(token) {
//   jwt.verify(token, process.env.JWT,function(err,payload){
//     if(err) return err;
//     return payload;
//   })
// }
function getDataFromToken(token) {
  try {
   return jwt.verify(token, process.env.JWT);
  } catch(err) {
    return err;
  }
}

/**
 * @desc Generate Auth token
 * @access Public
 */
administratorSchema.methods.generatePassToken = function () {
  const expiresIn = 60 * 60 * 24 * 7;// a week
  const token = jwt.sign({}, process.env.JWT, { expiresIn: expiresIn });
  this.resetPasswordToken = token;
  return token;
};

/**
 * @desc Generate Auth token
 * @access Public
 */
administratorSchema.methods.isStatusActif = function () {
  if(this.status) return true;
  return false;
};

const Administrator = new mongoose.model("Administrator", administratorSchema);
/**
 * @desc Validate SignUp
 * @access Public
 */
function validateAdmin(admin) {
  const schema = Joi.object({
    userName: Joi.string()
      .min(5)
      .max(30)
      .required(),
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
    bio: Joi.string()
      .min(5)
      .max(255),
    profileImage: Joi.string()
      .min(5)
      .max(255),
    password: passwordComplexity(complexityOptions),
  });
  return schema.validate(admin);
}

/**
 * @desc Validate SignUp
 * @access Public
 */
function validateAdminUpdate(admin) {
  const schema = Joi.object({
    userName: Joi.string()
      .min(5)
      .max(20),
    email: Joi.string()
      .max(255)
      .email(),
    firstName: Joi.string()
      .min(5)
      .max(15),
    lastName: Joi.string()
      .min(5)
      .max(15),
    bio: Joi.string()
      .min(10)
      .max(1024)
  });
  return schema.validate(admin);
}


/**
 * @desc Validate Email
 * @access Public
 */
function validateEmail(admin) {
  const schema = Joi.object({
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
  });

  return schema.validate(admin);
}
/**
 * @desc Validate SignIn
 * @access Public
 */
function validateSignIn(admin) {
  const schema = Joi.object({
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
    password: passwordComplexity(complexityOptions)
  });

  return schema.validate(admin);
}

/**
 * @desc Validate Password
 * @access Public
 */
function validatePassword(admin) {
  const schema = Joi.object({
    newPassword: passwordComplexity(complexityOptions),
    confirmedPassword: passwordComplexity(complexityOptions)
  });
  return schema.validate(admin);
}
/**
 * @desc Validate Password
 * @access Public
 */
function validatePasswordModification(admin) {
  const schema = Joi.object({
    password: passwordComplexity(complexityOptions).required(),
    newPassword: passwordComplexity(complexityOptions).required(),
    confirmedPassword: passwordComplexity(complexityOptions).required()
  });
  return schema.validate(admin);
}
/**
 * @Exports
 */
exports.getDataFromToken = getDataFromToken;
exports.hasExpiredorInvalid = hasExpiredorInvalid;
exports.validateAdminUpdate = validateAdminUpdate;
exports.validatePassword = validatePassword;
exports.validatePasswordModification = validatePasswordModification;
exports.validateAdmin = validateAdmin;
exports.validateEmail = validateEmail;
exports.validateSignIn = validateSignIn;
exports.Administrator = Administrator;
exports.administratorSchema = administratorSchema;
