const Joi = require("@hapi/joi");
const mongoose = require("mongoose");
const passwordComplexity = require("joi-password-complexity");
const jwt = require("jsonwebtoken");
const fs = require('fs')





  var Schema = mongoose.Schema;
const teacherSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      required: false,
      minLenght: 5,
      maxlength: 30
    },
    email: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 255,
      unique: true
    },

    bio: {
      type: String,
      required: false,
      minLenght: 10,
      maxlength: 1024
    },

    status: {
      type: Boolean,
      default:true
    },
  },
  { timestamps: true }
);

const Teacher = new mongoose.model("Teacher", teacherSchema);
/**
 * @desc Validate SignUp
 * @access Public
 */
function validateTeacher(admin) {
  const schema = Joi.object({
    userName: Joi.string()
      .min(5)
      .max(30)
      .required(),
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
      bio:Joi.string()
      .min(10)
      .max(200)
  });
  return schema.validate(admin);
}
exports.Teacher = Teacher;
exports.teacherSchema = teacherSchema;
exports.validateTeacher = validateTeacher;