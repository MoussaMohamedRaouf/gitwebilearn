const mongoose = require("mongoose");
const Joi = require("@hapi/joi");

const privilegeSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 50
    },
    description: {
        type: String,
        required: true,
        minLenght: 5,
        maxlength: 255
    },
    
  },
  { timestamps: true }
);
const Privilege = new mongoose.model("Privilege", privilegeSchema);

/**
 * Validate Priviege before adding or updating in Db 
 */
function validatePrivilege(privilege) {
  const schema = Joi.object({
    name: Joi.string()
      .min(5)
      .max(50)
      .required(),
    description:Joi.string()
      .min(5)
      .max(255)
      .required()
  });
  return schema.validate(privilege);
}
/**
 * Exports
 */
exports.validatePrivilege = validatePrivilege;
exports.privilegeSchema = privilegeSchema;
exports.Privilege = Privilege;
