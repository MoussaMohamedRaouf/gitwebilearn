const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
var Schema = mongoose.Schema;

const subscriptionSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 50
    },
    description: {
        type: String,
        required: true,
        minLenght: 5,
        maxlength: 255
    },
    cost:{
        type: Number,
        min: 0,
        max: 1000,
        required: true,

    },
    dureeAbonnement:{
      type: String,
      required: true,      
  }
  },
  { timestamps: true }
);
const Subscribtion = new mongoose.model("Subscribtion", subscriptionSchema);

/**
 * Validate Priviege before adding or updating in Db 
 */
function validateSubscribtion(subscription) {
  const schema = Joi.object({
    name: Joi.string()
      .min(5)
      .max(255)
      .required(),
    description:Joi.string()
      .min(5)
      .max(1024)
      .required(),
    cost:Joi.number()
    .required(),
    dureeAbonnement:Joi.string()
    .required()
  });
  return schema.validate(subscription);
}
/**
 * Exports
 */
exports.validateSubscribtion = validateSubscribtion;
exports.subscriptionSchema = subscriptionSchema;
exports.Subscribtion = Subscribtion;
