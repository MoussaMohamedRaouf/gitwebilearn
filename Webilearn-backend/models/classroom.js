const mongoose = require("mongoose");
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));
var Schema = mongoose.Schema;
const dateRegex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9] [AP][M]$/;



const classroomSchema = new mongoose.Schema(
  {
    titre: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 50
    },
    description: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 255
    },
    students: [{
      type: Schema.Types.ObjectId,
      ref: 'Student',
    }],
    tuteur: {
      type: Schema.Types.ObjectId,
      ref: 'Teacher',
      required: true,
    },
    date: {
      type: Date,
      required: true,
      min: '2020-01-01',
      max: '2099-12-31'
    },
    dureEnHeures: {
      type: String,
      required: true
    },
    dureEnMinutes: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

const Classroom = new mongoose.model("Classroom", classroomSchema);

function validateDate(dateToValidate) {
  const schema = Joi.object({
    
  });
  return schema.validate(dateToValidate);
}
function validateClassroom(classroom) {
  const schema = Joi.object({
    titre: Joi.string()
      .min(5)
      .max(50)
      .required(),
    description: Joi.string()
      .min(5)
      .max(255)
      .required(),

    date: Joi.date()
      .required(),

    heureDebut: Joi.string()
      .trim()
      .regex(dateRegex)
      .required(),


  });
  return schema.validate(classroom);
}
exports.validateClassroom = validateClassroom;
exports.validateDate = validateDate;
exports.classroomSchema = classroomSchema;
exports.Classroom = Classroom;