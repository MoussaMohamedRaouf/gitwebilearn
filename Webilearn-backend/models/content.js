const mongoose = require("mongoose");
const Joi = require("@hapi/joi");

const contentSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
            minLenght: 5,
            maxlength: 255
        },
        location: {
            type: String,
            minLenght: 5,
            maxlength: 512
        },
        description: {
            type: String,
            minLenght: 5,
            maxlength: 1024
        },
        mimetype:{
            type:String,
            required:true,
        }

    },
    { timestamps: true }
);
const Content = new mongoose.model("Content", contentSchema);

/**
 * Validate Content before adding or updating in Db 
 */
function validateContent(content) {
    const schema = Joi.object({
        title: Joi.string()
            .min(5)
            .max(255)
            .required(),
        location: Joi.string()
            .min(5)
            .max(255)
            .required(),
        description: Joi.string()
            .min(5)
            .max(1024)
    });
    return schema.validate(content);
}
/**
 * Exports
 */
exports.validateContent = validateContent;
exports.contentSchema = contentSchema;
exports.Content = Content;
