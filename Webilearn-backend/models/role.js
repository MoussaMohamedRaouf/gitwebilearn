const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
var Schema = mongoose.Schema;
const SuperAdminRoleId='5e922fb4272ac11af8111b53';

const roleSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 50
    },
    privileges: [{
      type: Schema.Types.ObjectId,
      ref: 'Privilege',
    }]
  },
  { timestamps: true }
);

const Role = new mongoose.model("Role", roleSchema);
/**
 * Validate Role before making Db modifications
 * @param {Role} role 
 */
function validateRole(role) {
  const schema = Joi.object({
    name: Joi.string()
      .min(5)
      .max(50)
      .required(),
    id:Joi.string()
      .min(5)
      .required()
  });
  return schema.validate(role);
}

  function validateRoleName(roleName) {
    const schema = Joi.object({
      name: Joi.string()
        .min(5)
        .max(50)
        .required()
    });
    return schema.validate(roleName);
  }

  function canHaveAccess(userRoleId,neededRoleId){
    if(userRoleId == neededRoleId) return true;
    return false;
  }
/**
 * Exports
 */
exports.SuperAdminRoleId = SuperAdminRoleId;
exports.validateRole = validateRole;
exports.canHaveAccess = canHaveAccess;
exports.validateRoleName = validateRoleName;
exports.roleSchema = roleSchema;
exports.Role = Role;
