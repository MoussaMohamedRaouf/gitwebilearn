const mongoose = require("mongoose");
const Joi = require("@hapi/joi");

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      minLenght: 6,
      maxlength: 20
    },
    
  },
  { timestamps: true }
);
const User = new mongoose.model("User", userSchema);
/**
 * Validate User before making Db modifications
 * @param {User} user 
 */
function validateUserName(user) {
  const schema = Joi.object({
    username: Joi.string()
      .min(6)
      .max(20)
      .required(),
  });
  return schema.validate(user);
}
/**
 * Exports
 */
exports.User = User;
exports.validateUserName = validateUserName;
exports.userSchema = userSchema;
