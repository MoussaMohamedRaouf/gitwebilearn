const mongoose = require("mongoose");
const Joi = require("@hapi/joi");
var Schema = mongoose.Schema;

const coursSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 50
    },
    description: {
      type: String,
      required: true,
      minLenght: 5,
      maxlength: 255
    },
    teacher: {
      type: Schema.Types.ObjectId,
      ref: 'Teacher',
      required: true,
    },
    price: {
      type: Number,
      min: 9,
      max: 99,
      default: 9
    },
    contents: [{
      type: Schema.Types.ObjectId,
      ref: 'Content'
    }],
  },
  { timestamps: true }
);
const Cours = new mongoose.model("Cours", coursSchema);


/**
 * Validate Priviege before adding or updating in Db 
 */
function validateCours(cours) {
  const schema = Joi.object({
    name: Joi.string()
      .min(5)
      .max(255)
      .required(),
    description: Joi.string()
      .min(5)
      .max(1024)
      .required(),
    teacher: Joi.string()
      .required(),
  });
  return schema.validate(cours);
}
/**
 * Exports
 */
exports.validateCours = validateCours;
exports.coursSchema = coursSchema;
exports.Cours = Cours;
