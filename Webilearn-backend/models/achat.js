const mongoose = require("mongoose");
const Joi = require('@hapi/joi');
var Schema = mongoose.Schema;


const achatSchema = new mongoose.Schema(
    { 
      student: {
        type: Schema.Types.ObjectId,
        ref: 'Student',
        required: true,
      },
      course: {
        type: Schema.Types.ObjectId,
        ref: 'Cours',
      },
    },
    { timestamps: true }
  );
  const Achat = new mongoose.model("Achat", achatSchema);






  exports.achatSchema = achatSchema;
  exports.Achat = Achat;