const jwt = require("jsonwebtoken");
module.exports = function auth(req, res, next) {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  if (!token)
    res.status(401).json({
      title: "Server",
      text: "Session expired please login again use this LinkToLoginPage to reconnect."
    });
  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
    try {
      const decoded = jwt.verify(token, process.env.JWT);
      req.user = decoded;
      next();
    } catch (error) {
      res.status(400).json({
        title: "Server",
        text: "Session expired please login again use this LinkToLoginPage to reconnect."
      });
    }
  } else {
    res.status(401).json({
      title: "Server",
      text: "Session expired please login again use this LinkToLoginPage to reconnect."
    });
  }
}

