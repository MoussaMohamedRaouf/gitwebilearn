const { SuperAdminRoleId, canHaveAccess } = require('../models/role');
module.exports = function role(req, res, next) {
    if (SuperAdminRoleId!==req.user.role) {
        res.status(403).json({
            title: "Server:",
            text: "Forbidden, you are not allowed to acces this page."
        });
    }
    next();
}