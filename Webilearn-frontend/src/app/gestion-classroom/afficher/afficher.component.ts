import { Component, OnInit } from '@angular/core';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { ClassroomService } from 'src/app/_services/classroom.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-afficher',
  templateUrl: './afficher.component.html',
  styleUrls: ['./afficher.component.css']
})
export class AfficherComponent implements OnInit {
  classRoomLoaded:any;
  id = this.route.snapshot.paramMap.get('id');

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private classroomService: ClassroomService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadOneClassRoom(this.id);
  }
  loadOneClassRoom(id){
    this.classroomService
    .loadClassRoom(id)
    .subscribe(
      res => {
        this.classRoomLoaded = res;
        console.log(this.classRoomLoaded);
      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
      
      );
  }
  modifyClassroom(){
    this.router.navigate(["/classrooms/modifier/"+this.id]);
  }
}
