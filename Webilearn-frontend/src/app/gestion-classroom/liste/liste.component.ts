import { ClassroomService } from './../../_services/classroom.service';
import { SweetAlertComponent } from './../../components/sweetalert/sweetalert.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {
  rows = [];
  temp = [];
  classRoomLoaded:any;
  @ViewChild(DatatableComponent, {static: false }) table: DatatableComponent;

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private classroomService: ClassroomService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.loadClassRooms();

  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    this.temp = val === ''? this.rows :this.rows.filter(function (d) {
      // .include()
      return d.description.toLowerCase().indexOf(val) !== -1 ||  d.titre.toLowerCase().indexOf(val) !== -1 || !val;
    });
    //  this.rows = temporairy;
    this.table.offset=0;
  }

  private loadClassRooms() {
    this.classroomService
      .loadClassRooms()
      .subscribe(
        courses => {
          console.log(courses);
          this.temp = courses;    
          this.rows = courses;    
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }
  deleteClassRoom(deleteButton) {
  Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteClassRoomCall(deleteButton.id);
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }
  private deleteClassRoomCall(id:string){
    this.classroomService
    .deleteClassRoom(id)
    .subscribe(
      res => {
        this.loadClassRooms();
      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
      
      );
    }

    modifyClassroom(modifyButton){
      this.router.navigate(["/classrooms/modifier/"+modifyButton.id]);
    }
    showClassroom(showButton){
      this.router.navigate(["/classrooms/afficher/"+showButton.id]);

    }
    
}
