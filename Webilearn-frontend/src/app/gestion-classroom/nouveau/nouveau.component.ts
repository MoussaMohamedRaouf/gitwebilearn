import { EnsegignantsService } from './../../_services/ensegignants.service';
import { EtudiantsService } from './../../_services/etudiants.service';
import { ClassroomService } from './../../_services/classroom.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormValidators } from 'src/app/_shared/form.validator';
import * as moment from 'moment';

@Component({
  selector: 'app-nouveau',
  templateUrl: './nouveau.component.html',
  styleUrls: ['./nouveau.component.css'],

})
export class NouveauComponent implements OnInit {
  form: FormGroup;
  now =moment().toDate();
  loadedStudents: any;
  loadedTeachers: any;
  selectedStudents:any[] = [];
  selectedHeure=['01','02','03','04'];
  selectedMinutes=['00','15','30','45'];
  id = this.route.snapshot.paramMap.get('id');
  course: any;
  constructor(
    private fb: FormBuilder,
    private sweetAlertComponent: SweetAlertComponent,
    private classroomService: ClassroomService,
    private etudiantsService: EtudiantsService,
    private ensegignantsService: EnsegignantsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadTeachers();
    this.loadStudents();
    this.formValidatorInit();
    if (this.id) {
      this.loadClassRoom(this.id);
    }
  }
  private formValidatorInit() {
    this.form = this.fb.group({
      students: ['',Validators.required],
      tuteur:['',Validators.required],
      date: ['',[Validators.required,FormValidators.dateMinimum(this.now.toString())]],
      heureDebut:['',Validators.required],
      dureEnHeures:['',Validators.required],
      dureEnMinutes:['',Validators.required],
      titre: ['', FormValidators.classRoomTitleValidator()],
      description: ['', FormValidators.courseDescVlaidator()],
    });
  }
  private loadClassRoom(id: string) {
    this.classroomService.loadClassRoom(id).subscribe(
      res => {
        console.log(res);
        res.students.forEach(student => {
          this.selectedStudents.push(student._id);
        });
        this.form.patchValue({
          titre: res.titre,
          description: res.description,
          date:res.date,
          students:this.selectedStudents,
          tuteur:res.tuteur._id,
          heureDebut:res.date,
          dureEnHeures:res.dureEnHeures,
          dureEnMinutes: res.dureEnMinutes,
        });
      },
      error => {
        this.router.navigate(['/classrooms/liste']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }

  
  private loadStudents() {
    this.etudiantsService.loadStudentsIds().subscribe(
      res => {
        this.loadedStudents = res;
      },
      error => {
        this.router.navigate(['/classrooms/list']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }
  private loadTeachers() {
    this.ensegignantsService.loadTeachersIds().subscribe(
      res => {
        this.loadedTeachers = res;
      },
      error => {
        this.router.navigate(['/classrooms/list']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }
  onSubmit() {
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('error', 'Form', 'Form informations are wrong please try again.');
      return;
    }
    let requestObject = this.form.value;
    if (this.id) {
      this.update(this.id,requestObject);
    } else {
      this.new(requestObject);
    }
  }


  private new(requestObject) {
    this.classroomService
      .createClassRoom(
        requestObject
      ).subscribe(
        newClassRoom => {
          this.router.navigate(['/classrooms/afficher/'+newClassRoom._id]);
          this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
        },
        error => {
          this.sweetAlertComponent.showSwal('error', "error", "error.text");
        }
      );
  }

  private update(id: string,requestObject) {
    this.classroomService
      .updateClassRoom(
        requestObject,
        id
      ).subscribe(
        res => {
          this.router.navigate(['/classrooms/afficher/'+id]);
          this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
        },
        error => {
          this.sweetAlertComponent.showSwal('error', "error", "error.text");
        }
      );
  }

  get titre() {
    return this.form.get("titre");
  }
  get tuteur() {
    return this.form.get("tuteur");
  }
  get students() {
    return this.form.get("students");
  }
  get date() {
    return this.form.get("date");
  }
  get description() {
    return this.form.get("description");
  }
  get systemeHoraire() {
    return this.form.get("systemeHoraire");
  }
  get heureDebut() {
    return this.form.get("heureDebut");
  }
  get dureEnHeures() {
    return this.form.get("dureEnHeures");
  }
  get dureEnMinutes() {
    return this.form.get("dureEnMinutes");
  }
}
