import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { classRoomRoute } from './gestion-classroom-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListeComponent } from './liste/liste.component';
import { NouveauComponent } from './nouveau/nouveau.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { NouisliderModule } from 'ng2-nouislider';
import { ApplicationPipesModule } from '../_pipes/application-pipes/application-pipes.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesModule } from 'angular-datatables';
import { MatNativeDateModule } from '@angular/material';
import { AfficherComponent } from './afficher/afficher.component';

@NgModule({
  declarations: [ListeComponent, NouveauComponent, AfficherComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(classRoomRoute),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    NouisliderModule,
    NgxDatatableModule,
    DataTablesModule,
    ApplicationPipesModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    NgxMaterialTimepickerModule
    
    
  ],

})
export class GestionClassroomModule { }
