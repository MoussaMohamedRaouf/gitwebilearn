import { AfficherComponent } from './afficher/afficher.component';
import { NouveauComponent } from './nouveau/nouveau.component';
import { ListeComponent } from './liste/liste.component';
import { Routes, RouterModule } from '@angular/router';


export const classRoomRoute: Routes = [
  {
    path: '',
    children: [
    {
      path: 'liste',
      component: ListeComponent
    }
    ]
  },{
    path: '',
    children: [
    {
      path: 'afficher/:id',
      component: AfficherComponent
    }
    ]
  },{
    path: '',
    children: [
    {
      path: 'nouveau',
      component: NouveauComponent
    },
    {
      path: 'modifier/:id',
      component: NouveauComponent
    }
    ]
  },
  
];