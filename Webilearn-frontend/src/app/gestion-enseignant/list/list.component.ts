import { Router } from '@angular/router';
import { SweetAlertComponent } from './../../components/sweetalert/sweetalert.component';
import { EnsegignantsService } from './../../_services/ensegignants.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  rows = [];
  temp = [];
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private ensegignantsService: EnsegignantsService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.loadTeachers();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    this.temp = val === '' ? this.rows : this.rows.filter(function (d) {
      // .include()
      return d.email.toLowerCase().indexOf(val) !== -1 || d.bio.toLowerCase().indexOf(val) !== -1 || !val;
    });
    //  this.rows = temporairy;
    this.table.offset = 0;
  }

  editEnseignant(editButton) {
    this.router.navigate(["/enseignants/update/" + editButton.id]);
  }
  changeStatus(statusButton) {
    let id = statusButton.id;
    this.ensegignantsService
      .changeStatus(id)
      .subscribe(
        res => {
          this.loadTeachers();
          this.sweetAlertComponent.showSwal('success', "Done", 'Administrator status changed');
        },
        error => {
          this.sweetAlertComponent.showSwal('warning', error.title, error.text);
        }
      );
  }

  private loadTeachers() {
    this.ensegignantsService
      .loadTeachers()
      .subscribe(
        teachers => {

          this.temp = teachers;
          this.rows = teachers;
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }


  private deleteEnsiegnantCall(id: string) {
    this.ensegignantsService
      .deleteTeacher(id)
      .subscribe(
        res => {
          this.loadTeachers();
        },
        error => {
          this.sweetAlertComponent.showSwal('warning', error.title, error.text);
        }

      );
  }





  deleteEnseignant(deleteButton) {


    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteEnsiegnantCall(deleteButton.id);
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }

}
