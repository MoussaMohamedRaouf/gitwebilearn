import { enseignantsRoute } from './gestion-enseignant-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesModule } from 'angular-datatables';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListComponent } from './list/list.component';
import { NewComponent } from './new/new.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { ApplicationPipesModule } from '../_pipes/application-pipes/application-pipes.module';

@NgModule({
  declarations: [ListComponent, NewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(enseignantsRoute),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    ApplicationPipesModule,
    NgxDatatableModule,
    DataTablesModule
  ]
})
export class GestionEnseignantModule { }
