import { FormValidators } from './../../_shared/form.validator';
import { EnsegignantsService } from './../../_services/ensegignants.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  id = this.route.snapshot.paramMap.get('id');
  enseignant:any;
  constructor(
    private fb:FormBuilder,
    private sweetAlertComponent:SweetAlertComponent,
    private ensegignantsService:EnsegignantsService,
    private router:Router,
    private route: ActivatedRoute
    ) { }

    private loadEnseignant(id: string) {
      this.ensegignantsService.loadTeacher(id).subscribe(
        res => {
          this.form.patchValue({
            email: res.email,
            userName: res.userName,
            bio: res.bio
          });
        },
        error => {
          this.router.navigate(['/enseignants/list']);
          this.sweetAlertComponent.showSwal('error', 'error', error.message);
        }
      );
    }
    /**
   * formValidator init
   */
  private formValidatorInit() {
    this.form = this.fb.group({
      userName: ["", FormValidators.userNameValidator()],
      bio: ["", FormValidators.bioValidator()],
      email: ["", FormValidators.emailInternValidator()
    ]
    });
  }
  ngOnInit() {
    this.formValidatorInit();
    if(this.id){
      this.loadEnseignant(this.id);
    }
  }


  onSubmit(){
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('error','Form','Form informations are wrong please try again.');
      return;
    }
    // const formData = new FormData();
    // formData.append('userName', this.userName.value);
    // formData.append('email', this.email.value);
    // formData.append('bio', this.bio.value);
    if(this.id){
      this.update(this.id);      
    }else{
      this.new();
    }
  }



  private new(){
    this.ensegignantsService
    .createTeacher(this.userName.value,this.email.value, this.bio.value).subscribe(
      res => {
        this.router.navigate(['/enseignants/list']);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
      },
      error => {
        this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }
  private update(id:string){
    this.ensegignantsService
    .updateTeacher(
      this.userName.value,this.email.value, this.bio.value,
      id
    ).subscribe(
      res => {
        this.router.navigate(['/enseignants/list']);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
      },
      error => {
        console.log(error);
        this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }



    /**
   * userName getter
   */
  get userName() {
    return this.form.get("userName");
  }
      /**
   * bio getter
   */
  get bio() {
    return this.form.get("bio");
  }

  /**
   * email getter
   */
  get email() {
    return this.form.get("email");
  }

}
