import { RoleGuard } from './_guards/role.guard';
import { Routes } from "@angular/router";
import { AdminLayoutComponent } from "./layouts/admin/admin-layout.component";
import { AuthLayoutComponent } from "./layouts/auth/auth-layout.component";
import { AuthGuard } from "./_guards/auth.guard";


const adminRoleId = '5e922fb4272ac11af8111b53';
export const AppRoutes: Routes = [
  {
    path: "",
    redirectTo: "dashboard",
    pathMatch: "full"
  },
  {
    path: "",
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        loadChildren: () =>
          import("./dashboard/dashboard.module").then(m => m.DashboardModule)
      },
      {
        path: "components",
        loadChildren: () =>
          import("./components/components.module").then(m => m.ComponentsModule)
      },
      {
        path: "forms",
        loadChildren: () => import("./forms/forms.module").then(m => m.Forms)
      },
      {
        path: "tables",
        loadChildren: () =>
          import("./tables/tables.module").then(m => m.TablesModule)
      },
      {
        path: "maps",
        loadChildren: () => import("./maps/maps.module").then(m => m.MapsModule)
      },
      {
        path: "widgets",
        loadChildren: () =>
          import("./widgets/widgets.module").then(m => m.WidgetsModule)
      },
      {
        path: "charts",
        loadChildren: () =>
          import("./charts/charts.module").then(m => m.ChartsModule)
      },
      {
        path: "calendar",
        loadChildren: () =>
          import("./calendar/calendar.module").then(m => m.CalendarModule)
      },
      {
        path: "",
        loadChildren: () =>
          import("./userpage/user.module").then(m => m.UserModule)
      },
      {
        path: "",
        loadChildren: () =>
          import("./timeline/timeline.module").then(m => m.TimelineModule)
      },
      {
        path: "courses",
        loadChildren: () =>
          import("./gestion-cours/gestion-cours.module").then(m => m.GestionCoursModule)
      },
      {
        path: "subscribtions",
        loadChildren: () =>
          import("./gestion-abonnement/gestion-abonnement.module").then(m => m.GestionAbonnementModule)
      },
      {
        path: "enseignants",
        loadChildren: () =>
          import("./gestion-enseignant/gestion-enseignant.module").then(m => m.GestionEnseignantModule)
      },
      {
        path: "etudiants",
        loadChildren: () =>
          import("./gestion-etudiant/gestion-etudiant.module").then(m => m.GestionEtudiantModule)
      },
      {
        path: "classrooms",
        loadChildren: () =>
          import("./gestion-classroom/gestion-classroom.module").then(m => m.GestionClassroomModule)
      },
      {
        path: "achats",
        loadChildren: () =>
          import("./gestion-achats/gestion-achats.module").then(m => m.GestionAchatsModule)
      },
      {
        path: "admins",
        canActivate: [RoleGuard], 
          data: { 
            expectedRole: adminRoleId
          }, 
        loadChildren: () =>
          import("./gestion-admin/gestion-admin.module").then(m => m.GestionAdminModule)
      },
    ]
  },
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      {
        path: "admin",
        loadChildren: () =>
          import("./pages/pages.module").then(m => m.PagesModule)
      }
    ]
  }
];
