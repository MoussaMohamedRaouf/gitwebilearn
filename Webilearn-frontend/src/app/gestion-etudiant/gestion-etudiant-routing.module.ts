import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { Routes } from '@angular/router';

export const etudiantsRoute: Routes = [
  {
    path: '',
    children: [
    {
      path: 'list',
      component: ListComponent
    }
    ]
  },{
    path: '',
    children: [
    {
      path: 'new',
      component: NewComponent
    },
    {
      path: 'update/:id',
      component: NewComponent
    }
    ]
  },
  
];