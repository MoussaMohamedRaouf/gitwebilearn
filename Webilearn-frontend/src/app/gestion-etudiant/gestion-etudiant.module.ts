import { NewComponent } from './new/new.component';
import { ListComponent } from './list/list.component';
import { etudiantsRoute } from './gestion-etudiant-routing.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { ApplicationPipesModule } from '../_pipes/application-pipes/application-pipes.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [ListComponent, NewComponent   ],
  imports: [
    CommonModule,
    RouterModule.forChild(etudiantsRoute),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    ApplicationPipesModule,
    NgxDatatableModule,
    DataTablesModule

  ]
})
export class GestionEtudiantModule { }
