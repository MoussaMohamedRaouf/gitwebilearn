import { FormValidators } from 'src/app/_shared/form.validator';
import { EtudiantsService } from './../../_services/etudiants.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  id = this.route.snapshot.paramMap.get('id');
  etudiant:any;
  constructor(
    private fb:FormBuilder,
    private sweetAlertComponent:SweetAlertComponent,
    private etudiantService:EtudiantsService,
    private router:Router,
    private route: ActivatedRoute
  ) { }
  private loadEtudiant(id: string) {
    this.etudiantService.loadOne(id).subscribe(
      res => {
        this.form.patchValue({
          email: res.email,
          userName: res.userName
        });
      },
      error => {
        this.router.navigate(['/etudiants/list']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }
   /**
   * formValidator init
   */
  private formValidatorInit() {
    this.form = this.fb.group({
      userName: ["", FormValidators.userNameValidator()],
      email: ["", FormValidators.emailInternValidator()
    ]
    });
  }
  ngOnInit() {
    this.formValidatorInit();
    if(this.id){
      this.loadEtudiant(this.id);
    }
  }
  onSubmit(){
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('error','Form','Form informations are wrong please try again.');
      return;
    }
    // const formData = new FormData();
    // formData.append('userName', this.userName.value);
    // formData.append('email', this.email.value);
    // formData.append('bio', this.bio.value);
    if(this.id){
      this.update(this.id);      
    }else{
      this.new();
    }
  }

  private new(){
    this.etudiantService
    .createStudent(this.userName.value,this.email.value).subscribe(
      res => {
        this.router.navigate(['/etudiants/list']);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Etudiant created successfully');
      },
      error => {
        this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }
  private update(id:string){
    this.etudiantService
    .updateStudent(
      this.userName.value,this.email.value,
      id
    ).subscribe(
      res => {
        this.router.navigate(['/etudiants/list']);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Etudiant updated successfully');
      },
      error => {
        console.log(error);
        this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }




    /**
   * userName getter
   */
  get userName() {
    return this.form.get("userName");
  }

  /**
   * email getter
   */
  get email() {
    return this.form.get("email");
  }

}
