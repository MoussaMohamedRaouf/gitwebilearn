import { Router } from '@angular/router';
import { EtudiantsService } from './../../_services/etudiants.service';
import { SweetAlertComponent } from './../../components/sweetalert/sweetalert.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  rows = [];
  temp = [];

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private etudiantsService: EtudiantsService,
    private router: Router
  ) { }


  ngOnInit() {
    this.loadStudents();
  }

  editEtudiant(editButton) {
    this.router.navigate(["/etudiants/update/" + editButton.id]);
  }
  changeStatus(statusButton) {
    let id = statusButton.id;
    this.etudiantsService
      .changeStatus(id)
      .subscribe(
        res => {
          this.loadStudents();
          this.sweetAlertComponent.showSwal('success', "Done", 'Administrator status changed');
        },
        error => {
          this.sweetAlertComponent.showSwal('warning', error.title, error.text);
        }
      );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    this.temp = val === '' ? this.rows : this.rows.filter(function (d) {
      // .include()
      return d.email.toLowerCase().indexOf(val) !== -1 || d.userName.toLowerCase().indexOf(val) !== -1 || !val;
    });
    //  this.rows = temporairy;
    this.table.offset = 0;
  }

  private loadStudents() {
    this.etudiantsService
      .loadStudents()
      .subscribe(
        students => {
          this.temp = students;
          this.rows = students;
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }



  private deleteEtudiantCall(id: string) {
    this.etudiantsService
      .deleteStudent(id)
      .subscribe(
        res => {
          this.loadStudents();
        },
        error => {
          this.sweetAlertComponent.showSwal('warning', error.title, error.text);
        }

      );
  }





  deleteEtudiant(deleteButton) {


    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteEtudiantCall(deleteButton.id);
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }


}
