import { Component, ElementRef, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { FormValidators } from "../../_shared/form.validator";
import { Administrator } from "../../_models/administrator";
import { AuthenticationService } from "./../../_services/authentication.service";

@Component({
  selector: "app-password-update",
  templateUrl: "./password-update.component.html",
  styleUrls: ["./password-update.component.css"]
})
export class PasswordUpdateComponent implements OnInit {
  test: Date = new Date();
  form: FormGroup;
  error = "";
  resetToken: null;
  loading = false;
  submitted = false;
  returnUrl: string;
  currentState: any;

  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;

  /**
   * @description PasswordUpdateComponent constructor
   * @param fb
   * @param element
   * @param authenticationService
   * @param router
   * @param route
   */
  constructor(
    public fb: FormBuilder,
    private element: ElementRef,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }
  /**
   * @description Verify token method
   */
  VerifyToken() {
    this.authenticationService.ValidPasswordToken(this.resetToken).subscribe(
      data => {
        console.log(data);
        this.currentState = "Verified";
      },
      err => {
        this.currentState = "NotVerified";
        console.log(err);
        this.router.navigate(["/"]);
      }
    );
  }
  /**
   * @description OnInit method
   */
  onInit() {
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName("navbar-toggle")[0];
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("login-page");
    body.classList.add("off-canvas-sidebar");
    const card = document.getElementsByClassName("card")[0];
    setTimeout(function() {
      // after 1000 ms we add the class animated to the login/register card
      card.classList.remove("card-hidden");
    }, 700);
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    this.currentState = "Wait";
    this.route.params.subscribe(params => {
      this.resetToken = params.token;
      this.VerifyToken();
    });

    this.formValidatorInit();
    this.nativeElement = this.element.nativeElement;
    this.sidebarVisible = false;
    // this.currentUserSubscription = this.authenticationService.token.subscribe(
    //   user => {
    //     this.currentUser = user;
    //   }
    // );
    // redirect to home if already logged in
    if (this.authenticationService.getToken()) {
      this.router.navigate(["/"]);
    }

  }

  private formValidatorInit() {
    this.form = this.fb.group({
      newPassword: ["", FormValidators.passwordValidator()],
      confirmedPassword: ["", FormValidators.passwordValidator()],
    }, { validator: FormValidators.passwordsShouldMatch });
  }

  sidebarToggle() {
    var toggleButton = this.toggleButton;
    var body = document.getElementsByTagName("body")[0];
    var sidebar = document.getElementsByClassName("navbar-collapse")[0];
    this.sidebarInAction(toggleButton, body);
  }

  private sidebarInAction(toggleButton: any, body: HTMLBodyElement) {
    if (this.sidebarVisible == false) {
      setTimeout(function() {
        toggleButton.classList.add("toggled");
      }, 500);
      body.classList.add("nav-open");
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove("toggled");
      this.sidebarVisible = false;
      body.classList.remove("nav-open");
    }
  }
  /**
   * @description NgOnInit function
   */
  ngOnInit() {
    this.onInit();
  }
  /**
   * @description Error Handler
   */
  handleError(error) {
    this.error = error.error.message;
  }
  /**
   * @description On submit function to call authService
   */
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.authenticationService
      .reset(
        this.newPassword.value,
        this.confirmedPassword.value,
        this.resetToken
      )
      .pipe(first())
      .subscribe(
        data => {
          console.log("succed");
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.handleError(error);
          this.loading = false;
        }
      );
  }
  /**
   * @description newPassword getter
   */
  get newPassword() {
    return this.form.get("newPassword");
  }
  /**
   * @description confirmedPassword getter
   */
  get confirmedPassword() {
    return this.form.get("confirmedPassword");
  }
}
