import { Component, ElementRef, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { first } from "rxjs/operators";
import { FormValidators } from "../../_shared/form.validator";
import { Administrator } from "../../_models/administrator";
import { AuthenticationService } from "./../../_services/authentication.service";
import { SweetAlertComponent } from "src/app/components/sweetalert/sweetalert.component";

@Component({
  selector: "admin-password-reset",
  templateUrl: "./password-reset.component.html",
  styleUrls: ["./password-reset.component.css"]
})
export class PasswordResetComponent implements OnInit, OnDestroy {
  test: Date = new Date();
  form: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;

  private formValidatorInit() {
    this.form = this.fb.group({
      email: ["", FormValidators.emailValidator()],
    });
  }

  /**
   * @description PasswordResetComponent constructor
   * @param element
   * @param authenticationService
   * @param router
   * @param route
   */
  constructor(
    private fb: FormBuilder,
    private element: ElementRef,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private sweetAlertComponent: SweetAlertComponent

  ) {
    
  }
  /**
   * @description ngOnInit method
   */
  ngOnInit() {
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName("navbar-toggle")[0];
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("login-page");
    body.classList.add("off-canvas-sidebar");
    const card = document.getElementsByClassName("card")[0];
    this.setTimeOut(card);
    this.formValidatorInit();
    this.nativeElement = this.element.nativeElement;
    this.sidebarVisible = false;
   
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }
  /**
   * @description card adding after 1 sec form page loading
   * @param card
   */
  private setTimeOut(card: Element) {
    setTimeout(function() {
      card.classList.remove("card-hidden");
    }, 700);
  }
  /**
   * @description side bar toogle
   */
  sidebarToggle() {
    var toggleButton = this.toggleButton;
    var body = document.getElementsByTagName("body")[0];
    var sidebar = document.getElementsByClassName("navbar-collapse")[0];
    if (this.sidebarVisible == false) {
      setTimeout(function() {
        toggleButton.classList.add("toggled");
      }, 500);
      body.classList.add("nav-open");
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove("toggled");
      this.sidebarVisible = false;
      body.classList.remove("nav-open");
    }
  }


  /**
   * @description On form submit function
   */
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.serviceCall();
  }
  /**
   * @description AuthService Call
   */
  private serviceCall() {
    this.authenticationService
      .recover(this.email.value)
      .pipe(first())
      .subscribe(
        res => {
          this.sweetAlertComponent.showSwal('success', res.title, res.text);
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
          this.loading = false;
        }
      );
  }

  /**
   * @description Email getter
   */
  get email() {
    return this.form.get("email");
  }
  /**
   * @description NgonDestroy method
   */
  ngOnDestroy() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("login-page");
    body.classList.remove("off-canvas-sidebar");
  }
}
