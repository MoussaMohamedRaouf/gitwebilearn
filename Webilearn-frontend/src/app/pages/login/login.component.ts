import { SweetAlertComponent } from './../../components/sweetalert/sweetalert.component';
import { Component, ElementRef, OnDestroy, OnInit } from "@angular/core";
import {  FormGroup, FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { first } from "rxjs/operators";
import { FormValidators } from "../../_shared/form.validator";
import { AuthenticationService } from "./../../_services/authentication.service";

declare var $: any;

@Component({
  selector: "app-login-cmp",
  templateUrl: "./login.component.html"
})
export class LoginComponent implements OnInit, OnDestroy {
  test: Date = new Date();
  form: FormGroup;
  error = "";
  loading = false;
  returnUrl: string;

  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;
  /**
   * Constructor for LoginComponent
   * @param element
   * @param authenticationService
   * @param router
   * @param route
   */
  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private element: ElementRef,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
  ) {
    this.init();
  }

  /**
   * Initialisation
   * @param element 
   */
  private init() {
    // redirect to home if already logged in
    if (this.authenticationService.getToken) {
      this.router.navigate(["/"]);
    }
  }

  /**
   * NgonInit function
   */
  ngOnInit() {
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName("navbar-toggle")[0];
    const body = document.getElementsByTagName("body")[0];
    body.classList.add("login-page");
    body.classList.add("off-canvas-sidebar");
    const card = document.getElementsByClassName("card")[0];
    setTimeout(function() {
      // after 1000 ms we add the class animated to the login/register card
      card.classList.remove("card-hidden");
    }, 700);
    this.nativeElement = this.element.nativeElement;
    this.sidebarVisible = false;

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    this.formValidatorInit();
  }
  /**
   * formValidator init
   */
  private formValidatorInit() {
    this.form = this.fb.group({
      email: ["", FormValidators.emailValidator()],
      password: ["", FormValidators.passwordValidator()]
    });
  }
  /**
   * @description
   */
  sidebarToggle() {
    var toggleButton = this.toggleButton;
    var body = document.getElementsByTagName("body")[0];
    var sidebar = document.getElementsByClassName("navbar-collapse")[0];
    if (this.sidebarVisible == false) {
      setTimeout(function() {
        toggleButton.classList.add("toggled");
      }, 500);
      body.classList.add("nav-open");
      this.sidebarVisible = true;
    } else {
      this.toggleButton.classList.remove("toggled");
      this.sidebarVisible = false;
      body.classList.remove("nav-open");
    }
  }
  /**
   * Ng destroy function
   */
  ngOnDestroy() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("login-page");
    body.classList.remove("off-canvas-sidebar");
  }

  /**
   * Event Listening and validation
   */
  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.login();
  }
  /**
   * Service Call
   */
  private login() {
    this.authenticationService
      .login(this.email.value, this.password.value)
      .pipe(first())
      .subscribe(
        res => {
          console.log("Connected.");
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
          this.loading = false;
        }
      );
  }
  
  /**
   * email getter
   */
  get email() {
    return this.form.get("email");
  }
  /**
   * password getter
   */
  get password() {
    return this.form.get("password");
  }
}
