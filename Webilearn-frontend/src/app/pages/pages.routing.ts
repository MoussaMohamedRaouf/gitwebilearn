import { PasswordUpdateComponent } from "./password-update/password-update.component";
import { Routes } from "@angular/router";

import { PasswordResetComponent } from "./password-reset/password-reset.component";
import { RegisterComponent } from "./register/register.component";
import { PricingComponent } from "./pricing/pricing.component";
import { LockComponent } from "./lock/lock.component";
import { LoginComponent } from "./login/login.component";

export const PagesRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "recover",
        component: PasswordResetComponent
      },
      {
        path: "reset/:token",
        component: PasswordUpdateComponent
      },
      {
        path: "lock",
        component: LockComponent
      },
      {
        path: "register",
        component: RegisterComponent
      },
      {
        path: "pricing",
        component: PricingComponent
      },
      {
        path: "",
        redirectTo: "/admin/login",
        component: LoginComponent
      }
    ]
  }
];
