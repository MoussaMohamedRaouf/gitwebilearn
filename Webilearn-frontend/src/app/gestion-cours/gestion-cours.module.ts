import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ApplicationPipesModule } from './../_pipes/application-pipes/application-pipes.module';
import { MaterialModule } from './../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { coursesRoute } from './gestion-cours-routing.module';
import { ListComponent } from './list/list.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesModule } from 'angular-datatables';
import { OneCoursComponent } from './one-cours/one-cours.component';
import { NewCoursComponent } from './new-cours/new-cours.component';
import { NewContentComponent } from './new-content/new-content.component';


@NgModule({
  declarations: [
    ListComponent,
    OneCoursComponent,
    NewCoursComponent,
    NewContentComponent
    
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(coursesRoute),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    DataTablesModule,
    ApplicationPipesModule,
    NgxExtendedPdfViewerModule
    
  ]
})
export class GestionCoursModule { }
