import { FormValidators } from 'src/app/_shared/form.validator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { CoursService } from 'src/app/_services/cours.service';

@Component({
  selector: 'app-new-content',
  templateUrl: './new-content.component.html',
  styleUrls: ['./new-content.component.css']
})
export class NewContentComponent implements OnInit {
  course:any;
  fileAcceptance = "video/*,application/pdf";
  
  id = this.route.snapshot.paramMap.get('id');
  contentId = this.route.snapshot.paramMap.get('contentId');
  
  form: FormGroup;
  constructor(
    private router:Router,
    private sweetAlertComponent:SweetAlertComponent,
    private coursService: CoursService,
    private route: ActivatedRoute,
    private fb:FormBuilder
    ) { }

  ngOnInit() {
    this.getCourse(this.id);
    this.formValidatorInit();
    if(this.contentId){
      this.getContent(this.id,this.contentId);
    }
  }
  private getContent(id:string,contentId:string){
    this.coursService
    .loadContent(id,contentId)
    .subscribe(
      content => {
        this.form.patchValue({
          title: content.title,
          description: content.description,
        });
      },
      error => {
        this.router.navigate(["/"]);
        this.sweetAlertComponent.showSwal('error', "Not found.", "Please double check the url or try again later.");
      }
    );
  }
  /**
   * formValidator init
   */
  private formValidatorInit() {
    this.form = this.fb.group({
      title: ["", FormValidators.courseTitleVlaidator()],
      description: ["", FormValidators.courseDescVlaidator()],
      content: ["", Validators.required],
    });
  }

  private getCourse(id:string){
    this.coursService
    .loadCourse(id)
    .subscribe(
      course => {
        this.course = course;    
      },
      error => {
        this.router.navigate(["/"]);
        this.sweetAlertComponent.showSwal('error', "Not found.", "Please double check the url or try again later.");
      }
    );
  }


  onSubmit(){
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('error','Form','Form informations are wrong please try again.');
      return;
    }
    const formData = this.createFormData();
    if(this.contentId){
      this.update(this.contentId,formData);
    }else{
      this.new(formData);
    }
  }
  private createFormData() {
    const formData = new FormData();
    formData.append('title', this.title.value);
    formData.append('description', this.description.value);
    const file_form = this.form.get('content').value;
    const file = file_form.files[0];
    formData.append('content', file);
    formData.forEach((value, key) => {
      console.log(key + ':' + value);
    });
    return formData;
  }

  private new(formData){
    this.coursService
    .createContent(this.course._id,formData).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/courses/show/'+this.course._id]);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
      },
      error => {
        console.log(error);
         this.sweetAlertComponent.showSwal('error', error.title, error.text);
      }
    );
  }
 private update(contentId:string,formData){
    this.coursService
    .updateContent(this.course._id,contentId,formData).subscribe(
      res => {
        this.router.navigate(['/courses/show/'+this.course._id]);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
      },
      error => {
        this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }
    /**
   * title getter
   */
  get title() {
    return this.form.get("title");
  }
  /**
   * description getter
   */
  get description() {
    return this.form.get("description");
  }
  /**
   * description getter
   */
  get content() {
    return this.form.get("content");
  }
}

