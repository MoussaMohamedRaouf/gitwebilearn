import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCoursComponent } from './new-cours.component';

describe('NewCoursComponent', () => {
  let component: NewCoursComponent;
  let fixture: ComponentFixture<NewCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
