import { EnsegignantsService } from './../../_services/ensegignants.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CoursService } from './../../_services/cours.service';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormValidators } from 'src/app/_shared/form.validator';

@Component({
  selector: 'app-new-cours',
  templateUrl: './new-cours.component.html',
  styleUrls: ['./new-cours.component.css']
})
export class NewCoursComponent implements OnInit {
  form: FormGroup;
  id = this.route.snapshot.paramMap.get('id');
  course:any;
  loadedTeachers: any;

  constructor(
    private fb:FormBuilder,
    private sweetAlertComponent:SweetAlertComponent,
    private coursService:CoursService,
    private ensegignantsService:EnsegignantsService,
    private router:Router,
    private route: ActivatedRoute
    ) { }
    private loadTeachers() {
      this.ensegignantsService.loadTeachersIds().subscribe(
        res => {
          this.loadedTeachers = res;
        },
        error => {
          this.router.navigate(['/courses/list']);
          this.sweetAlertComponent.showSwal('Erreur', 'Erreur', error.message);
        }
      );
    }
    private loadCours(id: string) {
      this.coursService.loadOne(id).subscribe(
        res => {
          console.log(res);
          this.form.patchValue({
            title: res.name,
            description: res.description,
            teacher: res.teacher._id,
          });
        },
        error => {
          this.router.navigate(['/courses/list']);
          this.sweetAlertComponent.showSwal('Erreur', 'Erreur', error.message);
        }
      );
    }
/**
   * formValidator init
   */
  private formValidatorInit() {
    this.form = this.fb.group({
      title: ["", FormValidators.courseTitleVlaidator()],
      description: ["", FormValidators.courseDescVlaidator()],
      teacher: ["", FormValidators.emailInternValidator()]
    });
  }
  ngOnInit() {
    this.loadTeachers();
    this.formValidatorInit();
    if(this.id){
      this.loadCours(this.id);
    }
  }
   
  onSubmit(){
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('Erreur','Formulaire mal saisie','Veuillez remplire le formulaire corréctement.');
      return;
    }
    if(this.id){
      this.update(this.id);      
    }else{
      this.new();
    }
  }
  private new(){
    this.coursService
    .createCours(
      this.title.value,
      this.description.value,
      this.teacher.value,
    ).subscribe(
      res => {
        this.router.navigate(['/courses/list']);
        this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
      },
      error => {
        console.log(error);
        this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }
  private update(id:string){
    this.coursService
    .updateCourse(
      this.title.value,
      this.description.value,
      this.teacher.value,
      id
    ).subscribe(
      res => {
         this.router.navigate(['/courses/list']);
         this.sweetAlertComponent.showSwal('success', 'Done.', 'Course updated successfully');
      },
      error => {
        console.log(error);
       // this.sweetAlertComponent.showSwal('error', error, error.text);
      }
    );
  }
  get title() {
    return this.form.get("title");
  }
  get teacher() {
    return this.form.get("teacher");
  }
  get description() {
    return this.form.get("description");
  }
}
