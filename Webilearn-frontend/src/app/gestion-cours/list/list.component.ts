import Swal from 'sweetalert2/dist/sweetalert2.js'
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CoursService } from './../../_services/cours.service';
import { SweetAlertComponent } from './../../components/sweetalert/sweetalert.component';
import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  rows = [];
  temp = [];
  @ViewChild(DatatableComponent, {static: false }) table: DatatableComponent;

  constructor( 
    private sweetAlertComponent: SweetAlertComponent,
    private coursService: CoursService,
    private router: Router,

    ) { 
      this.loadCourses();

    }

    modifyCourse(modifyButton){
      this.router.navigate(["/courses/update/"+modifyButton.id]);
    }

    visualizeCourse(editButton){
      this.router.navigate(["/courses/show/"+editButton.id]);
    }

    updateFilter(event) {
      const val = event.target.value.toLowerCase();
      this.temp = val === ''? this.rows :this.rows.filter(function (d) {
        // .include()
        return d.description.toLowerCase().indexOf(val) !== -1 ||  d.name.toLowerCase().indexOf(val) !== -1 || !val;
      });
      //  this.rows = temporairy;
      this.table.offset=0;
    }

    private loadCourses() {
      this.coursService
        .loadCourses()
        .subscribe(
          courses => {
            console.log(courses);
            this.temp = courses;    
            this.rows = courses;    
          },
          error => {
            this.sweetAlertComponent.showSwal('error', error.title, error.text);
          }
        );
    }

    private deleteCourseCall(id:string){
      this.coursService
      .deleteCourse(id)
      .subscribe(
        res => {
          console.log(res);
          this.loadCourses();
        },
        error => {
          this.sweetAlertComponent.showSwal('warning', error.title, error.text);
        }
        
        );
      }
    
    



    deleteCourse(deleteButton) {
  
    
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          console.log('confirmed!');
          this.deleteCourseCall(deleteButton.id);
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          console.log('canceled!');
  
          Swal.fire(
            'Cancelled',
            'No changes were made',
            'error'
          )
        }
      })
    }



    
  }
    