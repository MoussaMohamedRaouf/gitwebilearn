import { NewContentComponent } from './new-content/new-content.component';
import { NewCoursComponent } from './new-cours/new-cours.component';
import { OneCoursComponent } from './one-cours/one-cours.component';
import { ListComponent } from './list/list.component';
import { Routes } from '@angular/router';


export const coursesRoute: Routes = [
  {
    path: '',
    children: [
    {
      path: 'list',
      component: ListComponent
    }
    ]
  },
  {
    path: '',
    children: [
    {
      path: 'new',
      component: NewCoursComponent
    },
    {
      path: 'update/:id',
      component: NewCoursComponent
    }
    ]
  },
  {
    path: '',
    children: [
    {
      path: 'show/:id',
      component: OneCoursComponent
    }
    ]
  },
  {
    path: '',
    children: [
    {
      path: ':id/newContent',
      component: NewContentComponent
    },
    {
      path: ':id/update/:contentId',
      component: NewContentComponent
    }
    ]
  },

];