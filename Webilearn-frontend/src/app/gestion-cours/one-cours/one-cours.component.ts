import { environment } from './../../../environments/environment';
import { SweetAlertComponent } from './../../components/sweetalert/sweetalert.component';
import { CoursService } from './../../_services/cours.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-one-cours',
  templateUrl: './one-cours.component.html',
  styleUrls: ['./one-cours.component.css']
})
export class OneCoursComponent implements OnInit {
  course:any;
  videoUrl = { apiUrl: environment.coursesContent.toString() };
  id = this.route.snapshot.paramMap.get('id');

  constructor(
    private router:Router,
    private sweetAlertComponent:SweetAlertComponent,
    private coursService: CoursService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getCourse(this.id);
    
  }
  editContent(editButton){
    this.router.navigate(["courses/"+this.course._id+"/update/"+editButton.id]);
  }




  private getCourse(id:string){
    this.coursService
    .loadCourse(id)
    .subscribe(
      course => {
        this.course = course;    
        console.log(course);
      },
      error => {
        this.router.navigate(["/"]);
        this.sweetAlertComponent.showSwal('error', "Not found.", "Please double check the url or try again later.");
      }
    );
  }
  deleteContent(deleteButton) {
  Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteContentCall(this.course._id,deleteButton.id);
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        console.log('canceled!');

        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }
  private deleteContentCall(id:string,contentId:string){
    this.coursService
    .deleteContent(id,contentId)
    .subscribe(
      res => {
       
        this.getCourse(this.id);
      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
      );
    }

}
