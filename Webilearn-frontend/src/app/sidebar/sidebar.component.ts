import { environment } from './../../environments/environment';
import { Administrator } from './../_models/administrator';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: any[] = [{
        path: '/dashboard',
        title: 'Tableau de bord',
        type: 'link',
        icontype: 'dashboard'
    },{
        path: '/subscribtions',
        title: 'Abonnements',
        type: 'sub',
        icontype: 'how_to_reg',
        collapse: 'subscribtion',
        children: [
            {path: 'list', title: 'Liste des Abonnements', ab:'AL'},
            {path: 'new', title: 'Nouveau Abonnement', ab:'AN'},
        ]
    },{
        path: '/achats',
        title: 'Liste des achats',
        type: 'sub',
        icontype: 'euro_symbol',
        collapse: 'achats',
        children: [
           {path: 'courses', title: 'Sur les cours', ab:'AC'},
        ]
    },{
        path: '/admins',
        title: 'Administrateurs',
        type: 'sub',
        icontype: 'supervisor_account',
        collapse: 'admins',
        children: [
            {path: 'all', title: 'Liste Administrateurs', ab:'LA'},
            {path: 'new', title: 'Ajouter un admin', ab:'NA'},
        ]
    },{
        path: '/courses',
        title: 'Formations',
        type: 'sub',
        icontype: 'menu_book',
        collapse: 'courses',
        children: [
            {path: 'list', title: 'Liste des Cours', ab:'LC'},
            // {path: 'new', title: 'Ajouter un Cours', ab:'AC'},
        ]
    },{
        path: '/enseignants',
        title: 'Tuteurs',
        type: 'sub',
        icontype: 'school',
        collapse: 'enseignants',
        children: [
           {path: 'list', title: 'Liste des Tuteurs', ab:'AL'},
        //    {path: 'new', title: 'Nouveau Formateur', ab:'NE'},
        ]
    },{
        path: '/etudiants',
        title: 'Apprenants',
        type: 'sub',
        icontype: 'face',
        collapse: 'etudiants',
        children: [
           {path: 'list', title: 'Liste des Apprenants', ab:'AL'},
        //    {path: 'new', title: 'Nouveau Apprenant', ab:'NE'},
        ]
    },{
        path: '/classrooms',
        title: 'Class-rooms',
        type: 'sub',
        icontype: 'class',
        collapse: 'classrooms',
        children: [
           {path: 'liste', title: 'Liste des Class-room', ab:'LC'},
        //    {path: 'nouveau', title: 'Nouveau Class-room', ab:'NC'},
        ]
    }
    
    // ,{
    //     path: '/components',
    //     title: 'Components',
    //     type: 'sub',
    //     icontype: 'apps',
    //     collapse: 'components',
    //     children: [
    //         {path: 'buttons', title: 'Buttons', ab:'B'},
    //         {path: 'grid', title: 'Grid System', ab:'GS'},
    //         {path: 'panels', title: 'Panels', ab:'P'},
    //         {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
    //         {path: 'notifications', title: 'Notifications', ab:'N'},
    //         {path: 'icons', title: 'Icons', ab:'I'},
    //         {path: 'typography', title: 'Typography', ab:'T'}
    //     ]
    // },{
    //     path: '/forms',
    //     title: 'Forms',
    //     type: 'sub',
    //     icontype: 'content_paste',
    //     collapse: 'forms',
    //     children: [
    //         {path: 'regular', title: 'Regular Forms', ab:'RF'},
    //         {path: 'extended', title: 'Extended Forms', ab:'EF'},
    //         {path: 'validation', title: 'Validation Forms', ab:'VF'},
    //         {path: 'wizard', title: 'Wizard', ab:'W'}
    //     ]
    // },{
    //     path: '/tables',
    //     title: 'Tables',
    //     type: 'sub',
    //     icontype: 'grid_on',
    //     collapse: 'tables',
    //     children: [
    //         {path: 'regular', title: 'Regular Tables', ab:'RT'},
    //         {path: 'extended', title: 'Extended Tables', ab:'ET'},
    //         {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
    //     ]
    // },{
    //     path: '/maps',
    //     title: 'Maps',
    //     type: 'sub',
    //     icontype: 'place',
    //     collapse: 'maps',
    //     children: [
    //         {path: 'google', title: 'Google Maps', ab:'GM'},
    //         {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
    //         {path: 'vector', title: 'Vector Map', ab:'VM'}
    //     ]
    // },{
    //     path: '/widgets',
    //     title: 'Widgets',
    //     type: 'link',
    //     icontype: 'widgets'

    // },{
    //     path: '/charts',
    //     title: 'Charts',
    //     type: 'link',
    //     icontype: 'timeline'

    // },{
    //     path: '/calendar',
    //     title: 'Calendar',
    //     type: 'link',
    //     icontype: 'date_range'
    // },{
    //     path: '/pages',
    //     title: 'Pages',
    //     type: 'sub',
    //     icontype: 'image',
    //     collapse: 'pages',
    //     children: [
    //         {path: 'pricing', title: 'Pricing', ab:'P'},
    //         {path: 'timeline', title: 'Timeline Page', ab:'TP'},
    //         {path: 'login', title: 'Login Page', ab:'LP'},
    //         {path: 'register', title: 'Register Page', ab:'RP'},
    //         {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
    //         {path: 'user', title: 'User Page', ab:'UP'}
    //     ]
    // }
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    imagesUrl = { apiUrl: environment.profilePictures.toString() };

    public menuItems: any[];
    ps: any;
    currentUser:Administrator;
    constructor(private authenticationService: AuthenticationService) {
        this.currentUser =  this.authenticationService.getCurrentUserValue;
   }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
    }
    public isAllowedToSeeMenu(roleNeeded:string):boolean {
        if(this.currentUser.role===roleNeeded) return true;
        return false;
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
