import { ApplicationPipesModule } from './../_pipes/application-pipes/application-pipes.module';
import { NouisliderModule } from 'ng2-nouislider';
import { abonnementsRoute } from './gestion-abonnement-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListComponent } from './list/list.component';
import { MaterialModule } from '../app.module';
import { NewComponent } from './new/new.component';

@NgModule({
  declarations: [ListComponent, NewComponent   ],
  imports: [
    CommonModule,
    RouterModule.forChild(abonnementsRoute),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    NouisliderModule,
    ApplicationPipesModule

  ]
})
export class GestionAbonnementModule { }
