import Swal  from 'sweetalert2/dist/sweetalert2.js';
import { AbonnementsService } from './../../_services/abonnements.service';
import { Component, OnInit } from '@angular/core';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  abonnements:any;
  
  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private abonnementsService: AbonnementsService,
    private router: Router,

  ) {
    this.loadAbonnements();

   }
  
   private loadAbonnements(){
    this.abonnementsService
    .loadAbonnements()
    .subscribe(
      abonnements => {
      //  this.temp = courses;    
        this.abonnements = abonnements;    
        console.log(this.abonnements);
      },
      error => {
        this.router.navigate(["/"]);
        this.sweetAlertComponent.showSwal('error', error.title, error.text);
      }
    );
   }

  ngOnInit() {
  }
  editAbonnement(editButton){
    this.router.navigate(["/subscribtions/update/"+editButton.id]);
  }


  deleteAbonnement(deleteButton) {
  Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteAbonnementCall(deleteButton.id);
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        console.log('canceled!');

        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }
  private deleteAbonnementCall(id:string){
    this.abonnementsService
    .deleteAbonnement(id)
    .subscribe(
      res => {
       
        this.loadAbonnements();
      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
      );
    }

}
