import { AbonnementsService } from './../../_services/abonnements.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Router, ActivatedRoute } from '@angular/router';
import { FormValidators } from 'src/app/_shared/form.validator';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  simpleSlider = 199;
  id = this.route.snapshot.paramMap.get('id');
  subscribtion: any;
  dureeAbonnements=[
    {value:'7',viewValue:'7 jours'},
    {value:'30',viewValue:'1 mois'},
    {value:'90',viewValue:'3 mois'},
    {value:'180',viewValue:'6 mois'},
    {value:'365',viewValue:'1 ans'},
  ]
  constructor(
    private fb: FormBuilder,
    private sweetAlertComponent: SweetAlertComponent,
    private abonnementsService: AbonnementsService,
    private router: Router,
    private route: ActivatedRoute) { }

  private loadSubscribtion(id: string) {
    this.abonnementsService.loadOne(id).subscribe(
      res => {
        this.subscribtion = res;
        console.log(res);
        this.form.patchValue({
          title: res.name,
          description: res.description,
          cost: res.cost,
          dureeAbonnement: res.dureeAbonnement
        });
      },
      error => {
        console.log(error);
        this.router.navigate(['/subscribtions/list']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }

  private formValidatorInit() {
    this.form = this.fb.group({
      title: ["", FormValidators.courseTitleVlaidator()],
      description: ["", FormValidators.courseDescVlaidator()],
      cost: [""],
      dureeAbonnement:['',Validators.required],
    });
  }




  ngOnInit() {
    this.formValidatorInit();
    if (this.id) {
      this.loadSubscribtion(this.id);
    }
  }

  onSubmit() {
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('error', 'Form', 'Form informations are wrong please try again.');
      return;
    }
    if (this.id) {
      this.update();
    } else {
      this.new();
    }
  }
  private update() {
    this.abonnementsService
      .updateAbonnement(
        this.title.value,
        this.description.value,
        this.cost.value,
        this.dureeAbonnement.value,
        this.id
      ).subscribe(
        res => {
          this.router.navigate(['/subscribtions/list']);
          this.sweetAlertComponent.showSwal('success', 'Done.', 'Course updated successfully');
        },
        error => {
          console.log(error);
          this.sweetAlertComponent.showSwal('error', 'error', error.message);
        }
      );
  }
  private new() {
    this.abonnementsService
      .createAbonnement(
        this.title.value,
        this.description.value,
        this.cost.value,
        this.dureeAbonnement.value
      ).subscribe(
        res => {
  
          this.router.navigate(['/subscribtions/list']);
          this.sweetAlertComponent.showSwal('success', 'Done.', 'Course created successfully');
        },
        error => {
          console.log(error);
          this.sweetAlertComponent.showSwal('error', 'error', error.message);
        }
      );
  }
  /**
 * title getter
 */
  get title() {
    return this.form.get("title");
  }

  /**
   * description getter
   */
  get description() {
    return this.form.get("description");
  }


  /**
 * cost getter
 */
get cost() {
  return this.form.get("cost");
}  
get dureeAbonnement() {
  return this.form.get("dureeAbonnement");
}
}
