import { SellsService } from './../../_services/sells.service';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Router } from '@angular/router';
import dayGridPlugin from '@fullcalendar/daygrid';
import frLocale from '@fullcalendar/core/locales/fr';
import { FullCalendarComponent } from '@fullcalendar/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import { MatDatepickerInputEvent } from '@angular/material';

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.css']
})
export class CoursComponent implements OnInit {
  calendarPlugins = [dayGridPlugin];
  events =[];
  options: any;


  @ViewChild('fullcalendar',{ static: false }) fullcalendar: FullCalendarComponent;
  @Output()dateChange:EventEmitter<MatDatepickerInputEvent<any>>;

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private sellsService: SellsService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.options = {
      editable: true,
      theme: 'bootstrap', 
      header: {
        left: 'prev,next today myCustomButton',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      locales: [frLocale],
      locale: 'fr',
      // add other plugins
      plugins: [dayGridPlugin,timeGridPlugin]
    };
    this.loadAllSells();
  }



  someMethodName(date: any) {  
    console.log(date);
    let calendarApi = this.fullcalendar.getApi();
    calendarApi.gotoDate(date.value);
  }
  private loadAllSells(){
    this.sellsService.loadSells()
    .subscribe(
      sells => {
        this.events=sells;
        console.log(this.events);
      },
      error => {
        this.router.navigate(["/"]);
        this.sweetAlertComponent.showSwal('error', error.title, error.text);      }
    );
  }
  eventClick(model) {
    this.router.navigate(["/achats/afficher/"+model.event.id]);
  }

}
