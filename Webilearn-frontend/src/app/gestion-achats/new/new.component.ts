import { CoursService } from './../../_services/cours.service';
import { SellsService } from './../../_services/sells.service';
import { EnsegignantsService } from './../../_services/ensegignants.service';
import { EtudiantsService } from './../../_services/etudiants.service';
import { ClassroomService } from './../../_services/classroom.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormValidators } from 'src/app/_shared/form.validator';
import * as moment from 'moment';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  now =moment().toDate();
  loadedStudents: any;
  loadedCourses: any;
  id = this.route.snapshot.paramMap.get('id');
  achat: any;

  constructor(
    private fb: FormBuilder,
    private sweetAlertComponent: SweetAlertComponent,
    private sellsService: SellsService,
    private coursService: CoursService,
    private etudiantsService: EtudiantsService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadStudents();
    this.loadCourses();
    this.formValidatorInit();
    if (this.id) {
      this.loadAchat(this.id);
    }
  }

  private formValidatorInit() {
    this.form = this.fb.group({
      student: ['',Validators.required],
      course: ['',Validators.required],
    });
  }
  private loadAchat(id: string) {
    this.sellsService.loadSell(id).subscribe(
      res => {
        console.log(res);
        // this.form.patchValue({
        //   titre: res.titre,
        //   description: res.description,
        //   date:res.date,
        //   tuteur:res.tuteur,
        //   heureDebut:res.heureDebut,
        //   heureFin:res.heureFin
        // });
      },
      error => {
        this.router.navigate(['/achats/courses']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }
  private loadCourses() {
    this.coursService.loadCoursesIds().subscribe(
      res => {
        this.loadedCourses = res;
      },
      error => {
        this.router.navigate(['/achats/courses']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }
  private loadStudents() {
    this.etudiantsService.loadStudentsIds().subscribe(
      res => {
        this.loadedStudents = res;
      },
      error => {
        this.router.navigate(['/achats/courses']);
        this.sweetAlertComponent.showSwal('error', 'error', error.message);
      }
    );
  }
  onSubmit() {
    if (this.form.invalid) {
      this.sweetAlertComponent.showSwal('error', 'Form', 'Form informations are wrong please try again.');
      return;
    }

    let requestObject = this.form.value;
    console.log(requestObject);
    if (this.id) {
      this.update(this.id,requestObject);
    } else {
      this.new(requestObject);
    }
  }



  private new(requestObject) {
    this.sellsService
      .createSell(
        requestObject
      ).subscribe(
        newClassRoom => {
          this.sweetAlertComponent.showSwal('success', 'Done.', 'Sell created successfully');
        },
        error => {
          this.sweetAlertComponent.showSwal('error', "error", "error.text");
        }
      );
  }

  private update(id: string,requestObject) {
    this.sellsService
      .updateSell(
        requestObject.student,
        requestObject.course,
        id
      ).subscribe(
        res => {
          this.sweetAlertComponent.showSwal('success', 'Done.', 'Sell updated successfully');
        },
        error => {
          this.sweetAlertComponent.showSwal('error', "error", "error.text");
        }
      );
  }
  get course() {
    return this.form.get("course");
  }
  get student() {
    return this.form.get("student");
  }
}
