import { Component, OnInit } from '@angular/core';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { SellsService } from 'src/app/_services/sells.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  loadedSell:any;
  id = this.route.snapshot.paramMap.get('id');

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private sellsService: SellsService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.loadSell(this.id);
  }
  editSell(){
    this.router.navigate(["/achats/modifier/"+this.id]);

  }
  private deleteSellCall(){
    this.sellsService
    .deleteSell(this.id)
    .subscribe(
      res => {
        this.router.navigate(["/achats/courses"]);
        this.sweetAlertComponent.showSwal('warning', res.title, res.text);

      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
      );
    }
  

  deleteSell(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.deleteSellCall();
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }

  private loadSell(id){
    this.sellsService.loadSell(id)
    .subscribe(
      sell => {
        this.loadedSell=sell;
        console.log(sell);
      },
      error => {
        this.sweetAlertComponent.showSwal('error', error.title, error.text);      }
    );
  }
}
