import { ShowComponent } from './show/show.component';
import { NewComponent } from './new/new.component';
import { CoursComponent } from './cours/cours.component';

import { Routes } from '@angular/router';


export const achatsRoutes: Routes = [
 {
    path: '',
    children: [
    {
      path: 'courses',
      component: CoursComponent
    }
    ]
  },
  {
    path: '',
    children: [
    {
      path: 'nouveau',
      component: NewComponent
    },{
      path: 'modifier/:id',
      component: NewComponent
    }
    ]
  },
  {
    path: '',
    children: [
    {
      path: 'afficher/:id',
      component: ShowComponent
    }
    ]
  }
];