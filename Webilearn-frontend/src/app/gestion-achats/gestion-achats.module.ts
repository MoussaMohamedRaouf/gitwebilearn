import { achatsRoutes } from './gestion-achats-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursComponent } from './cours/cours.component';
import { AbonnementsComponent } from './abonnements/abonnements.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { NouisliderModule } from 'ng2-nouislider';
import { ApplicationPipesModule } from '../_pipes/application-pipes/application-pipes.module';
import { FullCalendarModule } from '@fullcalendar/angular';
import { NewComponent } from './new/new.component';
import { ShowComponent } from './show/show.component'; 


@NgModule({
  declarations: [CoursComponent, AbonnementsComponent, NewComponent, ShowComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(achatsRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    NouisliderModule,
    ApplicationPipesModule,
    FullCalendarModule,
  ]
})
export class GestionAchatsModule { }
