import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnsegignantsService {
  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }


  changeStatus(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}teachers/changeStatus/${id}`, {});
  }
  loadTeachers(){
    return this.http.get<any>(`${this.config.apiUrl}teachers/all`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadTeachersIds(){
    return this.http.get<any>(`${this.config.apiUrl}teachers/allIds`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadTeacher(id:string){
    return this.http.get<any>(`${this.config.apiUrl}teachers/`+id).pipe(map(res => {
      return res.data;
    })
    );
  }
  deleteTeacher(id:string){
    return this.http.post<any>(`${this.config.apiUrl}teachers/delete/`+id,{}).pipe(map(res => {
      return res.data;
    })
    );
  }
  createTeacher(userName:string,email:string, bio:string){
    return this.http.post<any>(`${this.config.apiUrl}teachers/new`,{ userName,email,bio }).pipe(map(res => {
      return res.data;
    })
    );
  }
  updateTeacher(userName:string,email:string, bio:string,id:string){
    return this.http.post<any>(`${this.config.apiUrl}teachers/update/`+ id, {userName,email,bio}).pipe(map(res => {
      return res.data;
    })
    );
  }
}
