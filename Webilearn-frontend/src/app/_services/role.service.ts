import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }
  loadRoles(id: string) {
    return this.http.get<any>(`${this.config.apiUrl}roles/allExceptMine/` + id).pipe(
      map(res => {
        let options = {};
        res.data.forEach((role) => options[role._id] = role.name);
        console.log(options);

        return options;
      })
    );

  }



  }
