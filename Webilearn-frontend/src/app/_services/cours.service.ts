import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoursService {
  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }


  loadCourses() {
    return this.http.get<any>(`${this.config.apiUrl}course/all`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadCoursesIds() {
    return this.http.get<any>(`${this.config.apiUrl}course/allIds`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadOne(id:string) {
    return this.http.get<any>(`${this.config.apiUrl}course/`+id).pipe(map(res => {
      return res.data;
    })
    );
  }
  createCours(name:string,description:string,teacher:string){
    return this.http.post<any>(`${this.config.apiUrl}course/new`,{name,description,teacher}).pipe(map(res => {
      return res.data;
    })
    );
  }
  
  updateCourse(name:string,description:string,teacher:string, id:string){
    return this.http.post<any>(`${this.config.apiUrl}course/update/`+id,{name,description,teacher}).pipe(map(res => {
      return res.data;
    })
    );
  }
  
  loadCourse(id:string){
    return this.http.get<any>(`${this.config.apiUrl}course/`+id+'/content/all').pipe(map(res => {
      return res.data;
    })
    );
  }

  deleteCourse(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}course/delete/`+id,{});
  }

  createContent(id:string,formdata:FormData){
    return this.http.post<any>(`${this.config.apiUrl}course/`+id+'/content/new',formdata).pipe(map(res => {
      return res.data;
    })
    );
  }
  deleteContent(id:string,contentId:string){
    return this.http.post<any>(`${this.config.apiUrl}course/`+id+'/content/delete/'+contentId,{}).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadContent(id:string,contentId:string){
    return this.http.get<any>(`${this.config.apiUrl}course/`+id+'/content/'+contentId).pipe(map(res => {
      return res.data;
    })
    );
  }
  
  updateContent( courseId:string,contentId:string,form:FormData ){
    return this.http.post<any>(`${this.config.apiUrl}course/`+courseId+`/content/update/`+contentId,form).pipe(map(res => {
      return res.data;
    })
    );
  }

}
