import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { id } from '@swimlane/ngx-datatable';

@Injectable({
  providedIn: 'root'
})
export class AbonnementsService {
  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }
  loadAbonnements(){
    return this.http.get<any>(`${this.config.apiUrl}subscribtions/all`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadOne(id:string){
    return this.http.get<any>(`${this.config.apiUrl}subscribtions/`+id).pipe(map(res => {
      return res.data;
    })
    );
  }
  deleteAbonnement(id:string){
    return this.http.post<any>(`${this.config.apiUrl}subscribtions/delete/`+id,{}).pipe(map(res => {
      return res.data;
    })
    );
  }
  createAbonnement(name:string,description:string,cost:string, dureeAbonnement:string){
    return this.http.post<any>(`${this.config.apiUrl}subscribtions/new`,{name, description, cost,dureeAbonnement }).pipe(map(res => {
      return res.data;
    })
    );
  }
  updateAbonnement(name:string,description:string,cost:string,dureeAbonnement:string,id:string){
    return this.http.post<any>(`${this.config.apiUrl}subscribtions/update/`+ id, { name, description, cost, dureeAbonnement }).pipe(map(res => {
      return res.data;
    })
    );
  }
}
