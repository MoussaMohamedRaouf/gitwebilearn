import { AuthenticationService } from 'src/app/_services/authentication.service';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "./../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class EditProfileService {
  public config = { apiUrl: environment.url };
  constructor(private http: HttpClient,
    private authenticationService:AuthenticationService) {
  }

  loadProfile(id: string) {
    return this.http.get<any>(`${this.config.apiUrl}administrator/me`);
  }

  updatePasswordInformations(password: string, newPassword: string, confirmedPassword: string) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/updatePassword`, {password,newPassword,confirmedPassword});
  }
  saveFirstLoginInfo(form:FormData) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/firstSignIn`,form)
    .pipe(
      map(
        res => {
          console.log(res);
          localStorage.removeItem("token");
          this.authenticationService.setToken(res.data);
          return res;
        },
        err => {
          return err;
        })
    );
  }
  updateProfileInformations(userName: string, firstName: string, lastName: string, bio: string) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/update`, { userName, firstName, lastName, bio });
  }
}
