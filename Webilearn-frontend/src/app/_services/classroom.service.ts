import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {
  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }


  loadClassRooms() {
    return this.http.get<any>(`${this.config.apiUrl}classrooms/all`).pipe(map(res => {
      return res.data;
    })
    );
  }
  
  loadClassRoom(id:string) {
    return this.http.get<any>(`${this.config.apiUrl}classrooms/`+id).pipe(map(res => {
      return res.data;
    })
    );
  }
  createClassRoom(requestObject:any){
    return this.http.post<any>(`${this.config.apiUrl}classrooms/new`, requestObject).pipe(map(res => {
      return res.data;
    })
    );
  }
  updateClassRoom(requestObject:any, id:string){
    return this.http.post<any>(`${this.config.apiUrl}classrooms/update/${id}`, requestObject).pipe(map(res => {
      return res.data;
    })
    );
  }
  deleteClassRoom(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}classrooms/delete/${id}`,{}).pipe(map(res => {
      return res.data;
    })
    );
  }
}
