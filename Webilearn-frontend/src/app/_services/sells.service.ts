import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SellsService {

  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }

  loadSells() {
    return this.http.get<any>(`${this.config.apiUrl}achats/all`).pipe(
      map(res => {
        const newArray = [];
        res.data.map(function (item) {
          newArray.push({ title: item.student.userName, date: item.createdAt, id:item._id });
        });
        return newArray;

      })
    );
  }

  loadSell(id:string) {
    return this.http.get<any>(`${this.config.apiUrl}achats/`+id).pipe(map(res => {
      return res.data;
    })
    );
  }

  createSell(requestObject:any){
    return this.http.post<any>(`${this.config.apiUrl}achats/new`, requestObject).pipe(map(res => {
      return res.data;
    })
    );
  }
  updateSell(studentId:string, courseId:string, id:string){
    return this.http.post<any>(`${this.config.apiUrl}achats/update/${id}`, {studentId,courseId}).pipe(map(res => {
      return res.data;
    })
    );
  }
  deleteSell(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}achats/delete/${id}`,{}).pipe(map(res => {
      return res.data;
    })
    );
  }
}