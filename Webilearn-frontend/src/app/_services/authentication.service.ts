import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { Injectable, OnInit } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "./../../environments/environment";
import { Administrator } from "../_models/administrator";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: "root"
})
export class AuthenticationService  {
  /** Global properties */
  public config = { apiUrl: environment.url };
  private currentUserSubject: BehaviorSubject<any> = new BehaviorSubject({});
  public currentUser: Observable<Administrator>;
  public helper = new JwtHelperService();

  constructor(private http: HttpClient, private router:Router) {
  }

  public get getCurrentUser():any {
    return this.currentUserSubject.asObservable();
  }
 
  /**
   * getter
   */
  getToken(): string {
    return localStorage.getItem('token');
  }

  public get getCurrentUserValue():Administrator {
    const currentUser =  this.helper.decodeToken(this.getToken());
    return currentUser;
  }
  /**
   * setter
   */
  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  public isTokenExpired(token?: string): boolean {
    if (!token) token = this.getToken();
    if (!token) return true;
    return this.helper.isTokenExpired(token);
  }

  /**
   * @description Login Administrator
   * @param email
   * @param password
   */
  login(email: string, password: string) {
    return this.http
      .post<any>(`${this.config.apiUrl}administrator/signIn`, {
        email,
        password
      })
      .pipe(
        map(
          res => {
            this.saveToken(res.data);
            const decodedToken = this.helper.decodeToken(res.data);
            if(decodedToken.firstConnection) {this.router.navigate(["/firstSignIn"]);}
            else { this.router.navigate(["/dashboard"]); } 
          },
          err => {
            return err;
          })
      );
  }
  /**
   * @description Call the back to validate a given token
   * @param token
   * @param password
   */
  private verifySessionToken(token: string): Observable<any> {
    return this.http
      .post<any>(`${this.config.apiUrl}administrator/verifySessionToken`, { token })
      .pipe(
        map(
          res => {
            return res;
          },
          err => {
            return err;
          })
      );
  }
  /**
   * @description validate user and its token
   * @param user
   */
  private saveToken(token: any) {
    if (token) {
      localStorage.setItem("token", token);
    }
  }
  /**
   * @description Logout user kill localstorge
   */
  logout() {
    localStorage.removeItem("token");
    this.currentUserSubject.next(null);
  }

  


  /**
   * @description Recover function to post email and put token on
   * @param email
   */
  recover(email: string) {
    return this.http
      .post<any>(`${this.config.apiUrl}administrator/recover`, { email })
      .pipe(
        map(res => {
          return res;
        })
      );
  }
  /**
   * @description Reset password callback api to modify informations
   * @param newPassword
   * @param confirmedPassword
   * @param token
   */
  reset(newPassword: string, confirmedPassword: string, token: string) {
    return this.http
      .post<any>(`${this.config.apiUrl}administrator/reset/` + token, {
        newPassword,
        confirmedPassword
      })
      .pipe(
        map(res => {
          return res;
        })
      );
  }
  /**
   * @description Validate if token provided is valid.
   * @param token
   */
  ValidPasswordToken(token: string): Observable<any> {
    return this.http.post(`${this.config.apiUrl}administrator/validateResetToken`, { token })
      .pipe(
        map(res => {
          return res;
        })
      );
  }
}
