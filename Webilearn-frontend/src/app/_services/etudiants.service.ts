import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EtudiantsService {
  public config = { apiUrl: environment.url };

  constructor(private http: HttpClient) { }


  changeStatus(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}students/changeStatus/${id}`, {});
  }
  loadStudents(){
    return this.http.get<any>(`${this.config.apiUrl}students/all`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadStudentsIds(){
    return this.http.get<any>(`${this.config.apiUrl}students/withIds`).pipe(map(res => {
      return res.data;
    })
    );
  }
  loadOne(id:string){
    return this.http.get<any>(`${this.config.apiUrl}students/`+id).pipe(map(res => {
      return res.data;
    })
    );
  }
  deleteStudent(id:string){
    return this.http.post<any>(`${this.config.apiUrl}students/delete/`+id,{}).pipe(map(res => {
      return res.data;
    })
    );
  }
  createStudent(userName:string,email:string){
    return this.http.post<any>(`${this.config.apiUrl}students/new`,{userName,email}).pipe(map(res => {
      return res.data;
    })
    );
  }
  
  updateStudent(userName:string,email:string,id:string){
    return this.http.post<any>(`${this.config.apiUrl}students/update/`+ id, {userName,email}).pipe(map(res => {
      return res.data;
    })
    );
  }








}
