import { map } from 'rxjs/operators';
import { environment } from "./../../environments/environment";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdministratorsService {
  public config = { apiUrl: environment.url };
  constructor(private http: HttpClient) {
  }

  createNewAdministrator(email: string, userName: string) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/new`, { email, userName });
  }
  /**
  * API call to load All administrators 
  */
  loadAdmins() {
    return this.http.get<any>(`${this.config.apiUrl}administrator/all`).pipe(map(res => {
      return res.data;
    })
    );
  }

  addRole(id: string, roleId: string) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/addRole/`+id, { roleId });
  }
  changeStatus(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/changeStatus`, {id});
  }

  deleteAdmin(id:string) {
    return this.http.post<any>(`${this.config.apiUrl}administrator/delete`, {id});
  }
}
