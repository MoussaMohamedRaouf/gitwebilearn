import { TestBed } from '@angular/core/testing';

import { EnsegignantsService } from './ensegignants.service';

describe('EnsegignantsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnsegignantsService = TestBed.get(EnsegignantsService);
    expect(service).toBeTruthy();
  });
});
