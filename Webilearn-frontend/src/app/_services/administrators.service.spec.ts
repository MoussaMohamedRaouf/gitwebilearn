import { TestBed } from '@angular/core/testing';

import { AdministratorsService } from './administrators.service';

describe('AdministratorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdministratorsService = TestBed.get(AdministratorsService);
    expect(service).toBeTruthy();
  });
});
