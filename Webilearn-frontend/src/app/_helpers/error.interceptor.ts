import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }
    /**
     * @description Intercept errors function
     * @param request 
     * @param next 
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if ((err.status === 401) || (err.status === 400)) {
                // auto logout if 401 response returned from api
                console.log('ErrorInterGotit');
                this.authenticationService.logout();
                location.reload(true);
            }
            const error = err.error;
            return throwError(error);
        }))
    }
}