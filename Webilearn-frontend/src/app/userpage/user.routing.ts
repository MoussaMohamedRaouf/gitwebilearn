import { FirstLoginGuard } from './../_guards/first-login.guard';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { FirstConnectionComponent } from './first-connection/first-connection.component';
import { Routes } from '@angular/router';

import { UserComponent } from './user.component';

export const UserRoutes: Routes = [
    {
      path: '',
      children: [ 
      {
        path: 'pages/user',
        component: UserComponent
      }
      ]
    },{
      path: '',
      canActivate: [FirstLoginGuard],

      children: [ 
      {
        path: 'firstSignIn',
        component: FirstConnectionComponent
      }
      ]
    },{
      path: '',
      children: [ 
      {
        path: 'me',
        component: MyProfileComponent
      }
      ]
    }
];
