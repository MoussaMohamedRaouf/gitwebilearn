import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { EditProfileService } from 'src/app/_services/edit-profile.service';
import { Administrator } from 'src/app/_models/administrator';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { FormValidators } from './../../_shared/form.validator';
// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, OnChanges, AfterViewInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormBuilder } from '@angular/forms';

declare const $: any;
interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: EventTarget;
  getMessage(): string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-first-connection',
  templateUrl: './first-connection.component.html',
  styleUrls: ['./first-connection.component.css']
})


export class FirstConnectionComponent implements OnInit, OnChanges, AfterViewInit {
  cities = [
    { value: 'tunis', viewValue: 'Tunis' },
    { value: 'sousse', viewValue: 'Sousse' },
    { value: 'sfax', viewValue: 'Sfax' }
  ];
  imageAcceptance = "image/x-png,image/gif,image/jpeg";
  currentUser: Administrator;
  administrator: Administrator;
  matcher = new MyErrorStateMatcher();
  form: FormGroup;
  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private editProfileService: EditProfileService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private router: Router,

  ) { }


  onFileSelect(event) {
    this.form.get('wizardPicture').setValue(event.target.files[0]);
  }

  private formValidation() {
    this.form = this.formBuilder.group({
      userName: ["", FormValidators.userNameValidator()],
      firstName: ["", FormValidators.firstNameValidator()],
      lastName: ["", FormValidators.lastNameValidator()],
      wizardPicture: [""],
      bio: ["", FormValidators.bioValidator()],
      newPassword: ["", FormValidators.passwordValidator()],
      confirmedPassword: ["", FormValidators.passwordValidator()],
      streetName: ["", FormValidators.streetNameValidator()],
      streetNumber: ["", FormValidators.streetNumberValidator()],
      city: ["", FormValidators.cityValidator()],
    }, { validator: FormValidators.passwordsShouldMatch }
    );
  }
  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    const formData = this.createFormData();
    this.updateInfo(formData);

  }
  private createFormData() {
    const formData = new FormData();
    formData.append('wizardPicture', this.wizardPicture.value);
    formData.append('userName', this.userName.value);
    formData.append('firstName', this.firstName.value);
    formData.append('lastName', this.lastName.value);
    formData.append('bio', this.bio.value);
    formData.append('newPassword', this.newPassword.value);
    formData.append('confirmedPassword', this.confirmedPassword.value);
    formData.append('streetName', this.streetName.value);
    formData.append('streetNumber', this.streetNumber.value);
    formData.append('city', this.city.value);
    return formData;
  }

  updateInfo(formData: FormData) {
    this.editProfileService
      .saveFirstLoginInfo(formData).pipe(first()).subscribe(
        res => {
          this.router.navigate(["/"]);
          this.sweetAlertComponent.showSwal('success', 'Server', 'informations registered');
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }
  private loadUser() {
    this.editProfileService
      .loadProfile(this.currentUser._id).pipe(first())
      .subscribe(
        res => {
          this.administrator = res;
          console.log(res);
          this.form.patchValue({
            
            userName: res.userName,
            //   description: res.description,
            //   date:res.date,
            //   tuteur:res.tuteur,
            //   heureDebut:res.heureDebut,
            //   heureFin:res.heureFin
            });
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }
  ngOnInit() {
    this.currentUser = this.authenticationService.getCurrentUserValue;
    this.loadUser();

    const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
    this.formValidation();

    // Code for the Validator
    const $validator = $('.card-wizard form').validate({
      rules: {
        firstname: {
          required: true,
          minlength: 3
        },
        lastname: {
          required: true,
          minlength: 3
        },
        profileImage: {
          required: true
        }
      },

      highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
      },
      success: function (element) {
        $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
      },
      errorPlacement: function (error, element) {
        $(element).append(error);
      }
    });

    // Wizard Initialization
    $('.card-wizard').bootstrapWizard({
      'tabClass': 'nav nav-pills',
      'nextSelector': '.btn-next',
      'previousSelector': '.btn-previous',

      onNext: function (tab, navigation, index) {
        var $valid = $('.card-wizard form').valid();
        if (!$valid) {
          $validator.focusInvalid();
          return false;
        }
      },

      onInit: function (tab: any, navigation: any, index: any) {

        // check number of tabs and fill the entire row
        let $total = navigation.find('li').length;
        let $wizard = navigation.closest('.card-wizard');

        let $first_li = navigation.find('li:first-child a').html();
        let $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
        $('.card-wizard .wizard-navigation').append($moving_div);

        $total = $wizard.find('.nav li').length;
        let $li_width = 100 / $total;

        let total_steps = $wizard.find('.nav li').length;
        let move_distance = $wizard.width() / total_steps;
        let index_temp = index;
        let vertical_level = 0;

        let mobile_device = $(document).width() < 600 && $total > 3;

        if (mobile_device) {
          move_distance = $wizard.width() / 2;
          index_temp = index % 2;
          $li_width = 50;
        }

        $wizard.find('.nav li').css('width', $li_width + '%');

        let step_width = move_distance;
        move_distance = move_distance * index_temp;

        let $current = index + 1;

        if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
          move_distance -= 8;
        } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
          move_distance += 8;
        }

        if (mobile_device) {
          let x: any = index / 2;
          vertical_level = parseInt(x);
          vertical_level = vertical_level * 38;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
        $('.moving-tab').css('transition', 'transform 0s');
      },

      onTabClick: function (tab: any, navigation: any, index: any) {

        const $valid = $('.card-wizard form').valid();

        if (!$valid) {
          return false;
        } else {
          return true;
        }
      },

      onTabShow: function (tab: any, navigation: any, index: any) {
        let $total = navigation.find('li').length;
        let $current = index + 1;
        elemMainPanel.scrollTop = 0;
        const $wizard = navigation.closest('.card-wizard');

        // If it's the last tab then hide the last button and show the finish instead
        if ($current >= $total) {
          $($wizard).find('.btn-next').hide();
          $($wizard).find('.btn-finish').show();
        } else {
          $($wizard).find('.btn-next').show();
          $($wizard).find('.btn-finish').hide();
        }

        const button_text = navigation.find('li:nth-child(' + $current + ') a').html();

        setTimeout(function () {
          $('.moving-tab').text(button_text);
        }, 150);

        const checkbox = $('.footer-checkbox');

        if (index !== 0) {
          $(checkbox).css({
            'opacity': '0',
            'visibility': 'hidden',
            'position': 'absolute'
          });
        } else {
          $(checkbox).css({
            'opacity': '1',
            'visibility': 'visible'
          });
        }
        $total = $wizard.find('.nav li').length;
        let $li_width = 100 / $total;

        let total_steps = $wizard.find('.nav li').length;
        let move_distance = $wizard.width() / total_steps;
        let index_temp = index;
        let vertical_level = 0;

        let mobile_device = $(document).width() < 600 && $total > 3;

        if (mobile_device) {
          move_distance = $wizard.width() / 2;
          index_temp = index % 2;
          $li_width = 50;
        }

        $wizard.find('.nav li').css('width', $li_width + '%');

        let step_width = move_distance;
        move_distance = move_distance * index_temp;

        $current = index + 1;

        if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
          move_distance -= 8;
        } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
          move_distance += 8;
        }

        if (mobile_device) {
          let x: any = index / 2;
          vertical_level = parseInt(x);
          vertical_level = vertical_level * 38;
        }

        $wizard.find('.moving-tab').css('width', step_width);
        $('.moving-tab').css({
          'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
          'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

        });
      }
    });


    // Prepare the preview for profile picture
    $('#wizardPicture').change(function () {
      const input = $(this);

      if (input[0].files && input[0].files[0]) {
        const reader = new FileReader();

        reader.onload = function (e: any) {
          $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        };
        reader.readAsDataURL(input[0].files[0]);
      }
    });

    $('[data-toggle="wizard-radio"]').click(function () {
      const wizard = $(this).closest('.card-wizard');
      wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
      $(this).addClass('active');
      $(wizard).find('[type="radio"]').removeAttr('checked');
      $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).find('[type="checkbox"]').removeAttr('checked');
      } else {
        $(this).addClass('active');
        $(this).find('[type="checkbox"]').attr('checked', 'true');
      }
    });

    $('.set-full-height').css('height', 'auto');

  }

  ngOnChanges(changes: SimpleChanges) {
    const input = $(this);

    if (input[0].files && input[0].files[0]) {
      const reader: any = new FileReader();

      reader.onload = function (e: any) {
        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
      };
      reader.readAsDataURL(input[0].files[0]);
    }
  }
  ngAfterViewInit() {

    $(window).resize(() => {
      $('.card-wizard').each(function () {
        setTimeout(() => {
          const $wizard = $(this);
          const index = $wizard.bootstrapWizard('currentIndex');
          let $total = $wizard.find('.nav li').length;
          let $li_width = 100 / $total;

          let total_steps = $wizard.find('.nav li').length;
          let move_distance = $wizard.width() / total_steps;
          let index_temp = index;
          let vertical_level = 0;

          let mobile_device = $(document).width() < 600 && $total > 3;
          if (mobile_device) {
            move_distance = $wizard.width() / 2;
            index_temp = index % 2;
            $li_width = 50;
          }

          $wizard.find('.nav li').css('width', $li_width + '%');

          let step_width = move_distance;
          move_distance = move_distance * index_temp;

          let $current = index + 1;

          if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
            move_distance -= 8;
          } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
            move_distance += 8;
          }

          if (mobile_device) {
            let x: any = index / 2;
            vertical_level = parseInt(x);
            vertical_level = vertical_level * 38;
          }

          $wizard.find('.moving-tab').css('width', step_width);
          $('.moving-tab').css({
            'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
            'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
          });

          $('.moving-tab').css({
            'transition': 'transform 0s'
          });
        }, 500)

      });
    });

  }
  /**
   * firstName getter
   */
  get firstName() {
    return this.form.get("firstName");
  }
  /**
  * lastName getter
  */
  get lastName() {
    return this.form.get("lastName");
  }
  /**
  * lastName getter
  */
  get wizardPicture() {
    return this.form.get("wizardPicture");
  }
  /**
  * lastName getter
  */
  get city() {
    return this.form.get("city");
  }
  /**
  * lastName getter
  */
  get streetNumber() {
    return this.form.get("streetNumber");
  }
  /**
  * lastName getter
  */
  get streetName() {
    return this.form.get("streetName");
  }
  get bio() {
    return this.form.get("bio");
  }
  get userName() {
    return this.form.get("userName");
  }
  /**
    * @description newPassword getter
    */
  get newPassword() {
    return this.form.get("newPassword");
  }
  /**
   * @description confirmedPassword getter
   */
  get confirmedPassword() {
    return this.form.get("confirmedPassword");
  }

}
