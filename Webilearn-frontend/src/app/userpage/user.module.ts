import { MaterialModule } from './../app.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UserComponent } from './user.component';
import { UserRoutes } from './user.routing';
import { FirstConnectionComponent } from './first-connection/first-connection.component';
import { MyProfileComponent } from './my-profile/my-profile.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserRoutes),
        FormsModule,
        MaterialModule,
        ReactiveFormsModule,
        
    ],
    declarations: [UserComponent, FirstConnectionComponent, MyProfileComponent]
})

export class UserModule {}
