import { first } from 'rxjs/operators';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { FormBuilder } from '@angular/forms';
import { EditProfileService } from 'src/app/_services/edit-profile.service';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { Administrator } from 'src/app/_models/administrator';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {
  data: any = null;
  imagesUrl = { apiUrl: environment.profilePictures.toString() }
  currentUser: Administrator;
  administrator: Administrator = new Administrator();

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private editProfileService: EditProfileService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.currentUser =  this.authenticationService.getCurrentUserValue;
    this.loadUser();
  }
  private loadUser() {
    this.editProfileService
      .loadProfile(this.currentUser._id).pipe(first())
      .subscribe(
        res => {
          this.administrator = res;
          console.log(this.administrator);
                      
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }

}
