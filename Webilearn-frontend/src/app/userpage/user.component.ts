import { Administrator } from './../_models/administrator';
import { SweetAlertComponent } from './../components/sweetalert/sweetalert.component';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { first } from 'rxjs/operators';
import { FormValidators } from './../_shared/form.validator';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EditProfileService } from 'src/app/_services/edit-profile.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-cmp',
  templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {
  data: any = null;
  form: FormGroup;
  imagesUrl = { apiUrl: environment.profilePictures.toString() }

  passwordForm: FormGroup;
  currentUser: Administrator;
  administrator: Administrator = new Administrator();
  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private fb: FormBuilder,
    private editProfileService: EditProfileService,
    private authenticationService: AuthenticationService
  ) {
    
  }
  ngOnInit() {
    this.currentUser =  this.authenticationService.getCurrentUserValue;
    this.loadUser();
    this.passwordFormValidation();
    this.formValidatorInit();
  }
  
  private loadUser() {
    this.editProfileService
      .loadProfile(this.currentUser._id).pipe(first())
      .subscribe(
        res => {
          this.administrator = res;                  
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }

  /**
   * formValidator init
   */
  private formValidatorInit() {
    this.form = this.fb.group({
      userName: ["", FormValidators.userNameValidator()],
      firstName: ["", FormValidators.firstNameValidator()],
      lastName: ["", FormValidators.lastNameValidator()],
      bio: ["", FormValidators.bioValidator()],
    });
  }
  /**
   * Password form validation function
   */
  private passwordFormValidation() {
    this.passwordForm = this.fb.group({
      password: ["", FormValidators.passwordValidator()],
      newPassword: ["", FormValidators.passwordValidator()],
      confirmedPassword: ["", FormValidators.passwordValidator()],
    }, { validator: FormValidators.passwordsShouldMatch });
  }
  /**
   *  Password change submitted function to call service
   */
  onPasswordSubmit() {
    if (this.passwordForm.invalid) {
      return;
    }
    this.editProfileService
      .updatePasswordInformations(
        this.password.value,
        this.newPassword.value,
        this.confirmedPassword.value
      ).pipe(first())
      .subscribe(
        res => {
          this.sweetAlertComponent.showSwal('success', res.title, res.text);

        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.update();
  }

  private update() {
    this.editProfileService
      .updateProfileInformations(
        this.userName.value,
        this.firstName.value,
        this.lastName.value,
        this.bio.value,
      ).pipe(first())
      .subscribe(
        res => {
          console.log(res);
          this.sweetAlertComponent.showSwal('success', res.title, res.text);
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error, error.text);
        }
      );
  }
  /**
   * email getter
   */
  get userName() {
    return this.form.get("userName");
  }

  /**
   * email getter
   */
  get firstName() {
    return this.form.get("firstName");
  }
  /**
   * lastName getter
   */
  get lastName() {
    return this.form.get("lastName");
  }
  /**
   * bio getter
   */
  get bio() {
    return this.form.get("bio");
  }
  /**
   * @description password getter
   */
  get password() {
    return this.passwordForm.get("password");
  }
  /**
   * @description newPassword getter
   */
  get newPassword() {
    return this.passwordForm.get("newPassword");
  }
  /**
   * @description confirmedPassword getter
   */
  get confirmedPassword() {
    return this.passwordForm.get("confirmedPassword");
  }

}
