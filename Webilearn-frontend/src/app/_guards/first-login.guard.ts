import { AuthenticationService } from './../_services/authentication.service';
import { SweetAlertComponent } from './../components/sweetalert/sweetalert.component';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class FirstLoginGuard implements CanActivate {
  constructor(
    private SweetAlertComponent:SweetAlertComponent,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  canActivate(): boolean  {

    // decode the token to get its payload
    const currentUser = this.auth.getCurrentUserValue;
    if (!currentUser.firstConnection) {
      this.SweetAlertComponent.showSwal('error', 'Server', 'You are not allowed to access this page.');
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
