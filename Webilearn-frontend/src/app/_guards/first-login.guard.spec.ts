import { TestBed, async, inject } from '@angular/core/testing';

import { FirstLoginGuard } from './first-login.guard';

describe('FirstLoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirstLoginGuard]
    });
  });

  it('should ...', inject([FirstLoginGuard], (guard: FirstLoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
