import { AuthenticationService } from "./../_services/authentication.service";
import { Injectable } from "@angular/core";
import {
  CanActivate,
  Router
} from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  /**
   * @description Constructor
   */
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate() {
    if (this.authenticationService.isTokenExpired()) {
      this.authenticationService.logout();
      this.router.navigate(["/admin/login"], { queryParams: { returnUrl: "/" } });
      return false;
    }
    return true;
  }
}
