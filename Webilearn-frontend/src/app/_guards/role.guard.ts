import { SweetAlertComponent } from './../components/sweetalert/sweetalert.component';
import { AuthenticationService } from './../_services/authentication.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(
    private SweetAlertComponent:SweetAlertComponent,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  canActivate(route: ActivatedRouteSnapshot): boolean  {
    const expectedRole = route.data.expectedRole;
    const token = localStorage.getItem('token');

    // decode the token to get its payload
    const currentUser = this.auth.getCurrentUserValue;
    if (this.auth.isTokenExpired() || currentUser.role !== expectedRole) {
      this.SweetAlertComponent.showSwal('error', 'Server', 'You are not allowed to access this page.');
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
  }

