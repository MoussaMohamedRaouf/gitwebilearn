import { RoleService } from './../_services/role.service';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { AdministratorsService } from './../_services/administrators.service';
import { Component, ViewChild } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-gestion-admin',
  templateUrl: './gestion-admin.component.html',
  styleUrls: ['./gestion-admin.component.css']
})
export class GestionAdminComponent  {
  rows = [];
  temp = [];

  options: {};

  @ViewChild(DatatableComponent, {static: false }) table: DatatableComponent;

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private administratorsService: AdministratorsService,
    private roleService: RoleService,
  ) { 
    this.loadAdmins();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    this.temp = val === ''? this.rows :this.rows.filter(function (d) {
      // .include()
      return d.email.toLowerCase().indexOf(val) !== -1 || d.userName.toLowerCase().indexOf(val) !== -1 || !val;
    });
    //  this.rows = temporairy;
    this.table.offset=0;
  }

  private loadAdmins() {
    this.administratorsService
      .loadAdmins()
      .subscribe(
        admins => {
          this.temp = admins;    
          this.rows = admins;    
          console.log(this.rows);
        },
        error => {
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }
 

  changeStatus(statusButton) {
    let id = statusButton.id;
    this.administratorsService
    .changeStatus(id)
    .subscribe(
      res => {
        this.loadAdmins();
       this.sweetAlertComponent.showSwal('success', "Done", 'Administrator status changed');
      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
    );
  }
  private addRole(roleId,userId){
    console.log(userId);
    this.administratorsService
    .addRole(userId,roleId)
    .subscribe(
      res => {
        console.log(res);
        Swal.fire(res.title, res.text,'success',);
      },
      error => {
        console.log(error);
        Swal.fire(
          error.title,
          error.text,
          'warning'
        );
      }
    );
  }
  private getUserRoles(id){
    this.roleService
    .loadRoles(id)
    .subscribe( 
      options => {
        this.options= options;
      }
    );
  }

  editAdmin(editButton: HTMLElement) {
    this.getUserRoles(editButton.id);
    Swal.fire({
      title: `Role to add or update for this administrator`,
      input: 'select',
      inputOptions: this.options,
      inputPlaceholder: 'Select a role',
      showCancelButton: true,
      inputValidator: (value) => {
        return new Promise((resolve) => {
          this.addRole(value,editButton.id);
          resolve();
        })
      }
    });
  }
  
  private deleteAdministrator(id:string){
    this.administratorsService
    .deleteAdmin(id)
    .subscribe(
      res => {
      this.loadAdmins();
      },
      error => {
        this.sweetAlertComponent.showSwal('warning', error.title, error.text);
      }
      
    );
  }
  deleteAdmin(deleteButton) {
  
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        console.log('confirmed!');
        this.deleteAdministrator(deleteButton.id);
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        console.log('canceled!');

        Swal.fire(
          'Cancelled',
          'No changes were made',
          'error'
        )
      }
    })
  }
}           

