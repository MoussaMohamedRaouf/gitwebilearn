import { FormValidators } from './../../_shared/form.validator';
import { Administrator } from 'src/app/_models/administrator';
import { AuthenticationService } from 'src/app/_services/authentication.service';
import { SweetAlertComponent } from 'src/app/components/sweetalert/sweetalert.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AdministratorsService } from './../../_services/administrators.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.css']
})
export class CreateAdminComponent implements OnInit {
  data: any = null;
  form: FormGroup;
  currentUser: Administrator;

  constructor(
    private sweetAlertComponent: SweetAlertComponent,
    private fb: FormBuilder,
    private administratorsService: AdministratorsService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.currentUser = this.authenticationService.getCurrentUserValue;
    this.formValidatorInit();
  }
  /**
  * formValidator init
  */
  private formValidatorInit() {
    this.form = this.fb.group({
      userName: ["", FormValidators.userNameValidator()],
      email: ["", FormValidators.emailInternValidator()],
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.createNewAdministrator();
  }
  /**
   * It creates calls endpoint to create new administrator
   */
  createNewAdministrator() {
    this.administratorsService
      .createNewAdministrator(this.email.value, this.userName.value)
      .subscribe(
        res => {
          this.sweetAlertComponent.showSwal('success', res.title, res.text);
        },
        error => {
          console.log(error);
          this.sweetAlertComponent.showSwal('error', error.title, error.text);
        }
      );
  }


  /**
   * email getter
   */
  get userName() {
    return this.form.get("userName");
  }
  /**
   * email getter
   */
  get email() {
    return this.form.get("email");
  }



}