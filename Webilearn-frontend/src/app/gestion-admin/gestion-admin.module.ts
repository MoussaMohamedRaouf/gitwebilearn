import { MaterialModule } from './../app.module';
import { GestionAdminComponent } from './gestion-admin.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesModule } from 'angular-datatables';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { adminRoute } from './gestion-admin-routing.module';
import { CreateAdminComponent } from './create-admin/create-admin.component';
@NgModule({
  declarations: [GestionAdminComponent, CreateAdminComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(adminRoute),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    DataTablesModule
    ]
})
export class GestionAdminModule { }
