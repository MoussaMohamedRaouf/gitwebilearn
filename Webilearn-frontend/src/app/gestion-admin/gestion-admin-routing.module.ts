import { CreateAdminComponent } from './create-admin/create-admin.component';
import { GestionAdminComponent } from './gestion-admin.component';
import { Routes } from '@angular/router';

export const adminRoute: Routes = [
  {
    path: '',
    children: [
    {
      path: 'all',
      component: GestionAdminComponent
    }
    ]
  },{
    path: '',
    children: [
    {
      path: 'new',
      component: CreateAdminComponent
    }
    ]
  },

];
