export class Administrator {
    _id: string;
    userName: string;
    email: string;
    company: string;
    firstName: string;
    lastName: string;
    profileImage: string;
    role:string;
    bio:string;
    firstConnection:boolean;
    
    constructor() {     
    }
    
    createAdministrator(_id: string,userName: string,email: string,company: string,firstName: string,lastName: string,profileImage: string,bio:string){
        this._id=_id;
        this.bio=bio;
        this.company=company;
        this.email=email;
        this.firstName=firstName;
        this.profileImage=profileImage;
        this.lastName=lastName;
        this.userName=userName;
    }

    
}

