export class User {
    _id: string;
    userName: string;
    email: string;
    company: string;
    firstName: string;
    lastName: string;
    profileImage: string;
    bio:string;
    firstConnection:boolean;
}

