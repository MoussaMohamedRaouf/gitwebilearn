import { first } from 'rxjs/operators';
import { FormValidators } from './../../_helpers/form.validator';
import { AuthService } from './../../_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  focus;
  focus1;
  focus2;
  form: FormGroup;
  closeResult: string;


    data : Date = new Date();

    constructor(
      private modalService: NgbModal,
      private authenticationService: AuthService,
      private router: Router,
      private route: ActivatedRoute,
      private fb: FormBuilder,
    ) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('signup-page');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-absolute');
        navbar.classList.remove('fixed-top');
        this.formValidatorInit();

    }
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('signup-page');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-absolute');
        navbar.classList.add('fixed-top');
      }
      private formValidatorInit() {
        this.form = this.fb.group({
          email: ["", FormValidators.emailValidator()],
          password: ["", FormValidators.passwordValidator()]
        });
      }
      onSubmit() {
        console.log("submitted");
        if (this.form.invalid) {
          return;
        }
        this.register();
      }
      private register() {
        this.authenticationService
          .login(this.email.value, this.password.value)
          .pipe(first())
          .subscribe(
            res => {
              console.log(res);
            },
            error => {
              console.log(error);

            }
          );
      }

      open(content, type, modalDimension) {
        if (modalDimension === 'sm' && type === 'modal_mini') {
            this.modalService.open(content, { windowClass: 'modal-mini modal-primary', size: 'sm' }).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        } else if (modalDimension === 'md' && type === 'Login') {
          this.modalService.open(content, { windowClass: 'modal-login modal-primary' }).result.then((result) => {
              this.closeResult = `Closed with: ${result}`;
          }, (reason) => {
              this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          });
        } else {
            this.modalService.open(content).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }

    }
    private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return  `with: ${reason}`;
      }
  }

  get email() {
    return this.form.get("email");
  }

  get password() {
    return this.form.get("password");
  }
  get userName() {
    return this.form.get("userName");
  }
}
