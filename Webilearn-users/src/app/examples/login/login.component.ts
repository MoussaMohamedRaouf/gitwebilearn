import { first } from 'rxjs/operators';
import { FormValidators } from './../../_helpers/form.validator';
import { AuthService } from './../../_services/auth.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    returnUrl: string;

    data: Date = new Date();
    focus;
    focus1;

    constructor(
        private element: ElementRef,
        private authenticationService: AuthService,
        private router: Router,
        private route: ActivatedRoute,
        private fb: FormBuilder,
    ) {
        
        this.init();
    }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');

        this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
        this.formValidatorInit();


    }
    ngOnDestroy() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }
    private init() {
        // redirect to home if already logged in
        if (this.authenticationService.getToken) {
            this.router.navigate(["/"]);
        }
    }
    private formValidatorInit() {
        this.form = this.fb.group({
            email: ["", FormValidators.emailValidator()],
            password: ["", FormValidators.passwordValidator()]
        });
    }
    
    onSubmit() {
        if (this.form.invalid) {
            console.log('submitted');
            return;

        }
        this.login();
    }
    
    private login() {
        this.authenticationService
            .login(this.email.value, this.password.value)
            .pipe(first())
            .subscribe(
                res => {
                    console.log(res);
                },
                error => {
                    console.log(error);

                }
            );
    }

    get email() {
        return this.form.get("email");
    }

    get password() {
        return this.form.get("password");
    }
}
