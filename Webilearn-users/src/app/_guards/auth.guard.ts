import { AuthService } from './../_services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthService
  ){}


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if (this.authenticationService.isTokenExpired()) {
      console.log('authGuard activated');
      this.authenticationService.logout();
      this.router.navigate(["/login"], { queryParams: { returnUrl: "/" } });

      return false;
    }
    return true;
  }
}
