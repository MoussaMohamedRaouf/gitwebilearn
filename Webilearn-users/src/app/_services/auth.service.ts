import { User } from './../_models/user';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from "./../../environments/environment";
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public config = { apiUrl: environment.url };
  public helper = new JwtHelperService();

  private currentUserSubject: BehaviorSubject<any> = new BehaviorSubject({});
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  public get getCurrentUser(): any {
    return this.currentUserSubject.asObservable();
  }
  public get getCurrentUserValue(): User {
    const currentUser = this.helper.decodeToken(this.getToken());
    return currentUser;
  }

  login(email: string, password: string) {
    return this.http
      .post<any>(`${this.config.apiUrl}user/signIn`, {
        email,
        password
      })
      .pipe(
        map(
          res => {
            this.saveToken(res.data);
            this.router.navigate(["/presentation"]);
          },
          err => {
            return err;
          })
      );
  }
  getToken(): string {
    return localStorage.getItem('token');
  }
  setToken(token: string): void {
    localStorage.setItem('token', token);
  }
  private saveToken(token: any) {
    if (token) {
      localStorage.setItem("token", token);
    }
  }

  logout() {
    localStorage.removeItem("token");
    this.currentUserSubject.next(null);
  }
  public isTokenExpired(token?: string): boolean {
    if (!token) token = this.getToken();
    if (!token) return true;
    return this.helper.isTokenExpired(token);
  }
}
