import { FormControl, Validators, ValidationErrors, ValidatorFn, FormGroup } from "@angular/forms";
import { AbstractControl } from "@angular/forms";



const pricePattern = /^[0-9]{2,3}(\.[0-9][0-9]?)?$/;
const streetPattern = /^[0-9]{1,3}$/;
const emailPattern = /^[a-zA-Z0-9\.\-]+@[a-z0-9\-]+\.[a-z]{2,3}$/;
const passwordPattern = /^(?=.*[A-Z])(?=.*[\W])(?=.*[0-9])(?=.*[a-z]).{8,30}$/;

export class FormValidators {
    /**
     * @description Remove spaces from a feild
     * @param feild
     */
    static removeSpaces(feild: FormControl) {
      if (feild && feild.value) {
        let removedSpaces = feild.value.split(" ").join("");
        feild.value !== removedSpaces && feild.setValue(removedSpaces);
      }
      return null;
    }

    static passwordsShouldMatch(control: AbstractControl) {
      return control.get("newPassword").value ===
        control.get("confirmedPassword").value
        ? null
        : { passwordsShouldMatch: true };
    }

    static emailInternValidator(){
      return [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50),
        FormValidators.removeSpaces
      ]
    }
    static emailValidator(){
      return [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern(emailPattern),
        FormValidators.removeSpaces
      ]
    }
    static passwordValidator(){
      return [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
      ]
    }
    static userNameValidator(){
      return [
        Validators.minLength(5),
        Validators.maxLength(20),
        Validators.required,
        FormValidators.removeSpaces
      ]
    }
    static lastNameValidator(){
      return [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10),
      ]
    }
    static firstNameValidator(){
      return [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10),
      ]
    }
    static bioValidator(){
      return [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(1024),
      ]
    }
    static streetNameValidator(){
      return [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
      ]
    }
    static streetNumberValidator(){
      return [
        Validators.required,
        Validators.pattern(streetPattern),
        FormValidators.removeSpaces
      ]
    }
    static cityValidator(){
      return [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(10),
        FormValidators.removeSpaces
      ]
    }
    static courseTitleVlaidator(){
      return [
        Validators.minLength(5),
        Validators.maxLength(30),
        Validators.required,
      ]
    }
    static courseDescVlaidator(){
      return [
        Validators.minLength(10),
        Validators.maxLength(1024),
        Validators.required
      ]
    }
    static priceValidator(){
      return [
        Validators.pattern(pricePattern),
      ]
    }
    static classRoomTitleValidator(){
      return [
        Validators.minLength(5),
        Validators.maxLength(30),
        Validators.required
      ]
    }
  }
  